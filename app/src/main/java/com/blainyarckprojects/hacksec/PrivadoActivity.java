package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class PrivadoActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView btn_publico;
    private ListView list_privado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privado);

        btn_publico = findViewById(R.id.btn_com);
        list_privado = findViewById(R.id.list_privado);

        btn_publico.setOnClickListener(this);

        int[] imgs = {R.drawable.icone_privado, R.drawable.icone_privado, R.drawable.icone_privado, R.drawable.icone_privado, R.drawable.icone_privado};
        String[] list_usuario = {"BlainyBot", "Pyrorubro", "NegoPreto", "xDuduAlp", "Brun0L0kao"};
        String[] list_mensagem = {"Mensagem recebida", "Mensagem recebida", "Mensagem recebida", "Mensagem recebida", "Mensagem recebida"};
        String[] list_horario = {"15:32", "14:45", "14:20", "14:15", "14:10"};

        MyAdapter adapter = new MyAdapter(this, imgs, list_usuario, list_mensagem, list_horario);

        list_privado.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_com:
                startActivity(new Intent(this, ComunicacaoActivity.class));
                finish();
            break;
        }
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        int list_img[];
        String list_usuario[];
        String list_mensagem[];
        String list_horario[];

        MyAdapter(Context c, int[] imgs, String[] usuario, String[] mensagem, String[] horario){
            super(c, R.layout.listview_privado, R.id.label_de, usuario);
            this.context = c;
            this.list_img = imgs;
            this.list_usuario = usuario;
            this.list_mensagem = mensagem;
            this.list_horario = horario;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_privado, parent, false);

            ImageView imgs = row.findViewById(R.id.img);
            TextView usuario = row.findViewById(R.id.label_usuario);
            TextView mensagem = row.findViewById(R.id.label_mensagem);
            TextView horario = row.findViewById(R.id.label_horario);


            imgs.setImageResource(list_img[position]);
            usuario.setText(list_usuario[position]);
            mensagem.setText(list_mensagem[position]);
            horario.setText(list_horario[position]);
            return row;
        }
    }
}
