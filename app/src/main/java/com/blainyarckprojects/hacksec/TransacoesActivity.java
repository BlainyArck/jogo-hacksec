package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class TransacoesActivity extends AppCompatActivity {

    private ImageView btn_rastros_rastros;
    ListView listview_transacoes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transacoes);

        int img_tipo[] = {R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais,
                R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais,
                R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais,
                R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais, R.drawable.icone_mais};

        String[] list_tipo = {"Deposito", "Deposito", "Deposito", "Deposito", "Deposito",
                "Deposito", "Deposito", "Deposito", "Deposito", "Deposito",
                "Deposito", "Deposito", "Deposito", "Deposito", "Deposito",
                "Deposito", "Deposito", "Deposito", "Deposito", "Deposito"};

        String[] list_data = {"Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46",
                "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46",
                "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46",
                "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46", "Sábado 29 de março 20:46"};

        String[] list_from = {"De:", "De:", "De:", "De:", "De:", "De:", "De:", "De:", "De:", "De:",
                "De:", "De:", "De:", "De:", "De:", "De:", "De:", "De:", "De:", "De:", };

        String[] list_para = {"DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z",
                "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z",
                "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z",
                "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z", "DOTF6CHIQV5Z"};

        String[] list_to = {"Para:", "Para:", "Para:", "Para:", "Para:", "Para:", "Para:", "Para:", "Para:", "Para:",
                "Para:", "Para:", "Para:", "Para:", "Para:", "Para:", "Para:", "Para:", "Para:", "Para:", };

        String[] list_de = {"UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3",
                "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3",
                "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3",
                "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3", "UJK456LH26K3"};

        String[] list_valores = {"$ 500", "$ 500", "$ 500", "$ 500", "$ 500", "$ 500", "$ 500", "$ 500", "$ 500", "$ 500",
                "$ 500", "$ 500", "$ 500", "$ 500", "$ 500", "$ 500", "$ 500", "$ 500", "$ 500", "$ 500"};

        listview_transacoes = findViewById(R.id.listview_transacoes);

        MyAdapter adapter = new MyAdapter(this, img_tipo, list_tipo, list_data, list_from, list_de,
                list_to, list_para, list_valores);

        listview_transacoes.setAdapter(adapter);

        btn_rastros_rastros = findViewById(R.id.btn_rastros_rastros);

        btn_rastros_rastros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TransacoesActivity.this, RastrosActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        int img_tipo[];
        String list_tipo[];
        String list_data[];
        String list_from[];
        String list_de[];
        String list_to[];
        String list_para[];
        String list_valores[];


        MyAdapter(Context c, int[] img, String[] tipo, String[] data, String[] from, String[] carteira_de,
                  String[] to, String[] carteira_para, String[] valores){
            super(c, R.layout.listview_transacoes, R.id.label_de, carteira_de);
            this.context = c;
            this.img_tipo = img;
            this.list_tipo = tipo;
            this.list_data = data;
            this.list_from = from;
            this.list_de = carteira_de;
            this.list_to = to;
            this.list_para = carteira_para;
            this.list_valores = valores;

        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_transacoes, parent, false);

            ImageView img = row.findViewById(R.id.img_tipo);
            TextView tipo = row.findViewById(R.id.label_dr);
            TextView data = row.findViewById(R.id.label_data);
            TextView from = row.findViewById(R.id.label_from);
            TextView carteira_de = row.findViewById(R.id.label_de);
            TextView to = row.findViewById(R.id.label_to);
            TextView carteira_para = row.findViewById(R.id.label_para);
            TextView valor = row.findViewById(R.id.label_valor);

            img.setImageResource(img_tipo[position]);
            tipo.setText(list_tipo[position]);
            data.setText(list_data[position]);
            from.setText(list_from[position]);
            carteira_de.setText(list_de[position]);
            to.setText(list_to[position]);
            carteira_para.setText(list_para[position]);
            valor.setText(list_valores[position]);
            return row;
        }
    }
}
