package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class EventosActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView btn_torneio, btn_rush, btn_heist, btn_bank, btn_cyber,
            btn_recompensat, btn_recompensar, btn_recompensah, btn_recompensab, btn_recompensacw;
    private TextView label_torneio, label_rush, label_heits, label_bank, label_cyber;
    Dialog eventos, recompensas, recompensa_bank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos);

        btn_torneio = findViewById(R.id.btn_torneio);
        btn_rush = findViewById(R.id.btn_rush);
        btn_heist = findViewById(R.id.btn_heist);
        btn_bank = findViewById(R.id.btn_bank);
        btn_cyber = findViewById(R.id.btn_cyber);

        btn_recompensat = findViewById(R.id.btn_recompensat);
        btn_recompensar = findViewById(R.id.btn_recompensar);
        btn_recompensah = findViewById(R.id.btn_recompensah);
        btn_recompensab = findViewById(R.id.btn_recompensab);
        btn_recompensacw = findViewById(R.id.btn_recompensacw);

        label_torneio = findViewById(R.id.label_torneio);
        label_rush = findViewById(R.id.label_rush);
        label_heits = findViewById(R.id.label_heist);
        label_bank = findViewById(R.id.label_bank);
        label_cyber = findViewById(R.id.label_cyber);

        eventos = new Dialog(this);
        eventos.setContentView(R.layout.layouteventos);
        eventos.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        recompensas = new Dialog(this);
        recompensas.setContentView(R.layout.layouteventosr);
        recompensas.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        recompensa_bank = new Dialog(this);
        recompensa_bank.setContentView(R.layout.layoutrecompensabank);
        recompensa_bank.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        label_torneio.setOnClickListener(this);
        label_rush.setOnClickListener(this);
        label_heits.setOnClickListener(this);
        label_bank.setOnClickListener(this);
        label_cyber.setOnClickListener(this);

        btn_torneio.setOnClickListener(this);
        btn_rush.setOnClickListener(this);
        btn_heist.setOnClickListener(this);
        btn_bank.setOnClickListener(this);
        btn_cyber.setOnClickListener(this);

        btn_recompensat.setOnClickListener(this);
        btn_recompensar.setOnClickListener(this);
        btn_recompensah.setOnClickListener(this);
        btn_recompensab.setOnClickListener(this);
        btn_recompensacw.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.label_torneio:
                mostrarTorneio();
            break;
            case R.id.label_rush:
                mostrarRush();
            break;
            case R.id.label_heist:
                mostrarHeist();
            break;
            case R.id.label_bank:
                mostrarBank();
            break;
            case R.id.label_cyber:
                mostrarCyber();
            break;
            case R.id.btn_torneio:
                startActivity(new Intent(this, EventoTorneioActivity.class));
            break;
            case R.id.btn_rush:
                startActivity(new Intent(this, EventoRushActivity.class));
            break;
            case R.id.btn_heist:
                startActivity(new Intent(this, EventoHeistActivity.class));
            break;
            case R.id.btn_bank:
                startActivity(new Intent(this, BankAttackActivity.class));
            break;
            case R.id.btn_cyber:
                startActivity(new Intent(this, EventoCyberwarActivity.class));
            break;
            case R.id.btn_recompensat:
                recompensaTorneio();
            break;
            case R.id.btn_recompensar:
                recompensaRush();
            break;
            case R.id.btn_recompensah:
                recompensaHeist();
            break;
            case R.id.btn_recompensab:
                recompensaBank();
            break;
            case R.id.btn_recompensacw:
                recompensaCyber();
            break;
        }
    }

    private void recompensaCyber() {
        Button btn_fechar;
        btn_fechar = recompensas.findViewById(R.id.btn_fechar);
        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recompensas.dismiss();
            }
        });
        recompensas.show();
    }

    private void recompensaBank() {
        Button btn_fechar;
        btn_fechar = recompensa_bank.findViewById(R.id.btn_fechar);
        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recompensa_bank.dismiss();
            }
        });
        recompensa_bank.show();
    }

    private void recompensaHeist() {
        Button btn_fechar;
        btn_fechar = recompensas.findViewById(R.id.btn_fechar);
        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recompensas.dismiss();
            }
        });
        recompensas.show();
    }

    private void recompensaRush() {
        Button btn_fechar;
        btn_fechar = recompensas.findViewById(R.id.btn_fechar);
        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recompensas.dismiss();
            }
        });
        recompensas.show();
    }

    private void recompensaTorneio() {
        Button btn_fechar;
        btn_fechar = recompensas.findViewById(R.id.btn_fechar);
        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recompensas.dismiss();
            }
        });
        recompensas.show();
    }

    private void mostrarCyber() {
        Button btn_fechar;
        btn_fechar = eventos.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventos.dismiss();
            }
        });

        ImageView img = eventos.findViewById(R.id.img_principal);
        TextView titulo = eventos.findViewById(R.id.label_titulo);
        TextView descricao = eventos.findViewById(R.id.label_descricao);

        img.setImageResource(R.drawable.icone_cyberwar);
        titulo.setText("Cyber War");
        descricao.setText("A Cyber War tem duração de 48h, a Equipe Hacker que conseguir derrubar mais dispositivos rivais, será a campeã.");

        eventos.show();
    }

    private void mostrarBank() {
        Button btn_fechar;

        btn_fechar = eventos.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventos.dismiss();
            }
        });

        ImageView img = eventos.findViewById(R.id.img_principal);
        TextView titulo = eventos.findViewById(R.id.label_titulo);
        TextView descricao = eventos.findViewById(R.id.label_descricao);

        img.setImageResource(R.drawable.icone_bankattack);
        titulo.setText("Bank Attack");
        descricao.setText("O Bank Attack tem duração de 30 min, para conseguir descobrir a senha do banco, é necessário que as 3 classes façam sua parte.");

        eventos.show();
    }

    private void mostrarHeist() {
        Button btn_fechar;

        btn_fechar = eventos.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventos.dismiss();
            }
        });

        ImageView img = eventos.findViewById(R.id.img_principal);
        TextView titulo = eventos.findViewById(R.id.label_titulo);
        TextView descricao = eventos.findViewById(R.id.label_descricao);

        img.setImageResource(R.drawable.icone_heist);
        titulo.setText("Heist");
        descricao.setText("O HEIST tem duração de 30min, todos os Hackers irão atacar a Exchange, os 100 melhores irão receber recompensas.");

        eventos.show();
    }

    private void mostrarRush() {
        Button btn_fechar;

        btn_fechar = eventos.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventos.dismiss();
            }
        });

        ImageView img = eventos.findViewById(R.id.img_principal);
        TextView titulo = eventos.findViewById(R.id.label_titulo);
        TextView descricao = eventos.findViewById(R.id.label_descricao);

        img.setImageResource(R.drawable.icone_rush);
        titulo.setText("RUSH");
        descricao.setText("O RUSH tem a duração de 1h, a Equipe Hacker que desenvolver mais aplicativos durante o periodo, será a campeã.");

        eventos.show();
    }

    private void mostrarTorneio() {
        Button btn_fechar;

        btn_fechar = eventos.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventos.dismiss();
            }
        });

        ImageView img = eventos.findViewById(R.id.img_principal);
        TextView titulo = eventos.findViewById(R.id.label_titulo);
        TextView descricao = eventos.findViewById(R.id.label_descricao);

        img.setImageResource(R.drawable.icone_torneio);
        titulo.setText("Torneio Diário");
        descricao.setText("O torneio diário dura 24h, o Hacker com mais reputação ganha durante esse periodo, será o campeão.");

        eventos.show();
    }
}
