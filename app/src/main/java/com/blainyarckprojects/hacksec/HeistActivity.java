package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class HeistActivity extends AppCompatActivity {

    ListView listview_heist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heist);

        listview_heist = findViewById(R.id.listview_cyber);

        String [] list_usuario = {"BlainyArck", "BlainyArck", "BlainyArck"};
        String [] list_dano = {"10", "10", "10"};

        MyAdapter adapter = new MyAdapter(this, list_usuario, list_dano);

        listview_heist.setAdapter(adapter);
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        String list_usuario[];
        String list_dano[];

        MyAdapter(Context c, String[] usuario, String[] dano){
            super(c, R.layout.listview_heist, R.id.label_usuario, usuario);
            this.context = c;
            this.list_usuario = usuario;
            this.list_dano = dano;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_heist, parent, false);

            TextView usuario = row.findViewById(R.id.label_usuario);
            TextView dano = row.findViewById(R.id.label_dano);

            usuario.setText(list_usuario[position]);
            dano.setText(list_dano[position]);

            return row;
        }
    }
}
