package com.blainyarckprojects.hacksec;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ProcuradosLegendsActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView btn_hackers, btn_grupohackers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_procurados_legends);

        btn_hackers = findViewById(R.id.btn_hackers);
        btn_grupohackers = findViewById(R.id.btn_grupohackers);

        btn_hackers.setOnClickListener(this);
        btn_grupohackers.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_hackers:
                startActivity(new Intent(this, ProcuradosActivity.class));
                finish();
            break;
            case R.id.btn_grupohackers:
                startActivity(new Intent(this, ProcuradosGrupoHackerActivity.class));
                finish();
            break;
        }
    }
}
