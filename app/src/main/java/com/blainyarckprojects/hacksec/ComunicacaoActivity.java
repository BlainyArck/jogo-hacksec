package com.blainyarckprojects.hacksec;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ComunicacaoActivity extends AppCompatActivity {

    private ImageView btn_privado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comunicacao);

        btn_privado = findViewById(R.id.btn_privado);

        btn_privado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ComunicacaoActivity.this, PrivadoActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
