package com.blainyarckprojects.hacksec;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class InicioActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView btn_ferramentas;
    private ImageView btn_dispositivo;
    private ImageView btn_desafios;
    private ImageView btn_server;
    private ImageView btn_grupo_hacker;
    private ImageView btn_procurados;
    private ImageView btn_mercado_negro;
    private ImageView btn_rede;
    private ImageView btn_rastros;
    private ImageView btn_carteira;
    private ImageView btn_comunicacao;
    private ImageView btn_eventos;
    private TextView label_level, label_usuario,
            label_money, label_ip, label_conexao;

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        btn_dispositivo = findViewById(R.id.btn_dispositivo);
        btn_ferramentas = findViewById(R.id.btn_ferramentas);
        btn_desafios = findViewById(R.id.btn_desafios);
        btn_server = findViewById(R.id.btn_server);
        btn_grupo_hacker = findViewById(R.id.btn_grupo_hacker);
        btn_procurados = findViewById(R.id.btn_procurados);
        btn_mercado_negro = findViewById(R.id.btn_mercado_negro);
        btn_rede = findViewById(R.id.btn_rede);
        btn_rastros = findViewById(R.id.btn_rastros);
        btn_carteira = findViewById(R.id.btn_carteira);
        btn_comunicacao = findViewById(R.id.btn_comunicacao);
        btn_eventos = findViewById(R.id.btn_eventos);
        label_level = findViewById(R.id.label_level);
        label_usuario = findViewById(R.id.label_usuario);
        label_money = findViewById(R.id.label_grupo_hacker);
        label_ip = findViewById(R.id.label_ip);
        label_conexao = findViewById(R.id.label_conexao);

        BuscarUsuario();

        btn_dispositivo.setOnClickListener(this);
        btn_ferramentas.setOnClickListener(this);
        btn_desafios.setOnClickListener(this);
        btn_server.setOnClickListener(this);
        btn_grupo_hacker.setOnClickListener(this);
        btn_procurados.setOnClickListener(this);
        btn_mercado_negro.setOnClickListener(this);
        btn_rede.setOnClickListener(this);
        btn_rastros.setOnClickListener(this);
        btn_carteira.setOnClickListener(this);
        btn_comunicacao.setOnClickListener(this);
        btn_eventos.setOnClickListener(this);
        label_level.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_dispositivo:
                startActivity(new Intent(this, DispositivoActivity.class));
                break;
            case R.id.btn_ferramentas:
                startActivity(new Intent(this, FerramentasAtaqueActivity.class));
                break;
            case R.id.btn_desafios:
                startActivity(new Intent(this, DesafiosActivity.class));
                break;
            case R.id.btn_server:
                startActivity(new Intent(this, ServerActivity.class));
                break;
            case R.id.btn_grupo_hacker:
                    mDatabase = FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    mDatabase.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            String grupohacker = dataSnapshot.child("grupohacker").getValue().toString();

                            if (grupohacker.equals("")){
                                Intent intent = new Intent(InicioActivity.this,CriarGrupoHackerActivity.class);
                                startActivity(intent);
                            }else{
                                Intent intent = new Intent(InicioActivity.this,GrupoHackerActivity.class);
                                startActivity(intent);
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {}
                    });
                break;
            case R.id.btn_procurados:
                startActivity(new Intent(this, ProcuradosActivity.class));
                break;
            case R.id.btn_mercado_negro:
                startActivity(new Intent(this, TrabalhoActivity.class));
                break;
            case R.id.btn_rede:
                startActivity(new Intent(this, RedeActivity.class));
                break;
            case R.id.btn_rastros:
                startActivity(new Intent(this, RastrosActivity.class));
                break;
            case R.id.btn_carteira:
                startActivity(new Intent(this, CarteiraActivity.class));
                break;
            case R.id.btn_comunicacao:
                startActivity(new Intent(this, ComunicacaoActivity.class));
                break;
            case R.id.btn_eventos:
                startActivity(new Intent(this, EventosActivity.class));
                break;
            case R.id.label_level:
                startActivity(new Intent(this, PerfilActivity.class));
                break;
        }
    }

    private void BuscarUsuario() {
        mDatabase = FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String usuario = dataSnapshot.child("usuario").getValue().toString();
                String level = dataSnapshot.child("level").getValue().toString();
                String money = dataSnapshot.child("money").getValue().toString();
                String enderecoip = dataSnapshot.child("enderecoip").getValue().toString();
                String conexao = dataSnapshot.child("conexao").getValue().toString();

                label_usuario.setText(usuario);
                label_level.setText(level);
                label_money.setText(money);
                label_ip.setText(enderecoip);
                label_conexao.setText(conexao);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }
}
