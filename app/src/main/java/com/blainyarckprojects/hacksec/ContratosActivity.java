package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ContratosActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_mnegro_trabalho, btn_mnegro_mercado, btn_novo_contato;
    ListView listview_contratos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contratos);

        btn_mnegro_trabalho = findViewById(R.id.btn_mnegro_trabalho);
        btn_mnegro_mercado = findViewById(R.id.btn_mnegro_mercado);
        listview_contratos = findViewById(R.id.listview_contratos);
        btn_novo_contato = findViewById(R.id.btn_novo_contrato);

        int[] list_img = {R.drawable.icone_contrato, R.drawable.icone_contrato, R.drawable.icone_contrato, R.drawable.icone_contrato, R.drawable.icone_contrato};
        String [] list_ip = {"192.168.15.60", "192.168.15.60", "192.168.15.60", "192.168.15.60", "192.168.15.60"};
        int[] list_fazer = {R.drawable.icone_transferenciai, R.drawable.icone_transferenciai, R.drawable.icone_transferenciai, R.drawable.icone_transferenciai, R.drawable.icone_transferenciai};

        MyAdapter adapter = new MyAdapter(this, list_img, list_ip, list_fazer);

        listview_contratos.setAdapter(adapter);

        btn_mnegro_trabalho.setOnClickListener(this);
        btn_mnegro_mercado.setOnClickListener(this);
        btn_novo_contato.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_mnegro_trabalho:
                startActivity(new Intent(this, TrabalhoActivity.class));
                finish();
            break;
            case R.id.btn_mnegro_mercado:
                startActivity(new Intent(this, MercadoNegroActivity.class));
                finish();
            break;
            case R.id.btn_novo_contrato:
                startActivity(new Intent(this, NovoContratoActivity.class));
                finish();
            break;
        }
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        int list_img[];
        String list_ip[];
        int list_fazer[];

        MyAdapter(Context c, int[] imgs, String[] ips, int[] fazer){
            super(c, R.layout.listview_transacoes, R.id.label_de, ips);
            this.context = c;
            this.list_img = imgs;
            this.list_ip = ips;
            this.list_fazer = fazer;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_contratos, parent, false);

            ImageView imgs = row.findViewById(R.id.img);
            TextView ips = row.findViewById(R.id.label_ip);
            ImageView fazer = row.findViewById(R.id.img_fazer);

            imgs.setImageResource(list_img[position]);
            ips.setText(list_ip[position]);
            fazer.setImageResource(list_fazer[position]);
            return row;
        }
    }
}
