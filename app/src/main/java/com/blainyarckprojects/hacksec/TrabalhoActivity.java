package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class TrabalhoActivity extends AppCompatActivity {

    private Button btn_mnegro_contratos, btn_mnegro_mercado, btn_trabalhar;
    private ImageView btn_trabalho;
    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trabalho);

        btn_mnegro_contratos = findViewById(R.id.btn_mnegro_contratos);
        btn_mnegro_mercado = findViewById(R.id.btn_mnegro_mercado);
        btn_trabalho = findViewById(R.id.btn_trabalho);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layouttrabalho);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        btn_trabalhar = findViewById(R.id.btn_trabalhar);

        btn_mnegro_contratos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrabalhoActivity.this, ContratosActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_mnegro_mercado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrabalhoActivity.this, MercadoNegroActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_trabalho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });
                myDialog.show();
            }
        });
    }

}
