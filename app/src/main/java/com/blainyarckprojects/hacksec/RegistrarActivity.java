package com.blainyarckprojects.hacksec;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blainyarckprojects.hacksec.classes.ClasseFerramentasAtaque;
import com.blainyarckprojects.hacksec.classes.ClasseFerramentasDefesa;
import com.blainyarckprojects.hacksec.classes.ClassePerfil;
import com.blainyarckprojects.hacksec.classes.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;

public class RegistrarActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText reg_usuario, reg_email, reg_senha, reg_confirmar;
    private ProgressBar reg_progressbar;
    private Button reg_btn;
    private FirebaseAuth mAuth;
    private TextView fazerLogin;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        reg_usuario = findViewById(R.id.reg_usuario);
        reg_email = findViewById(R.id.reg_email);
        reg_senha = findViewById(R.id.reg_senha);
        reg_confirmar = findViewById(R.id.reg_confirmar);
        reg_progressbar = findViewById(R.id.reg_progressbar);
        reg_btn = findViewById(R.id.reg_btn);
        reg_progressbar.setVisibility(View.INVISIBLE);
        mAuth = FirebaseAuth.getInstance();
        fazerLogin = findViewById(R.id.label_login);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        fazerLogin.setOnClickListener(this);
        reg_btn.setOnClickListener(this);

    }

    private void CriarContadeUsuario(final String email, final String usuario, String senha) {
        mAuth.createUserWithEmailAndPassword(email,senha)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Integer level = 1;
                            Integer reputacao = 90;
                            String enderecoip = criarEnderecoIP();
                            String conexao = "1G";
                            Integer money = 500;
                            Integer btc = 100;
                            Integer boosters = 100;
                            String grupohacker = "";
                            String cargo = "";
                            String classe = "";
                            String carteira = criarCarteira(12);

                                RegistrarDados();
                                registrarUsuario(email, usuario, level, reputacao, enderecoip,
                                        conexao, money, btc, boosters, grupohacker, cargo, classe, carteira);
                        }else{
                            String resp = task.getException().toString();
                            Functions.opcoesErro(getBaseContext(), resp);

                            reg_btn.setVisibility(View.VISIBLE);
                            reg_progressbar.setVisibility(View.INVISIBLE);
                        }
                    }
                });
    }

    private void registrarUsuario(String email, String usuario, Integer level, Integer reputacao,
                                  String enderecoip, String conexao, Integer money, Integer btc,
                                  Integer boosters, String grupohacker, String cargo, String classe, String carteira) {
        User user = new User(
                email,
                usuario,
                level,
                reputacao,
                enderecoip,
                conexao,
                money,
                btc,
                boosters,
                grupohacker,
                cargo,
                classe,
                carteira
        );

        FirebaseDatabase.getInstance().getReference("Users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Registrado();
                }else{
                    showMessage("Não foi possível se registrar." + task.getException().getMessage());
                    reg_btn.setVisibility(View.VISIBLE);
                    reg_progressbar.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void RegistrarFerramentasAtaque(Integer exploit, Integer vpn, Integer decrypt, Integer ransomware, Integer mitm,
                                           Integer ddos, Integer botnet, Integer phishing, Integer deface, Integer custo){

        ClasseFerramentasAtaque ataque = new ClasseFerramentasAtaque(
                exploit, vpn, decrypt, ransomware, mitm, ddos, botnet, phishing, deface, custo
        );

        FirebaseDatabase.getInstance().getReference("FerramentasAtaque")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .setValue(ataque).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){

                }else{
                    showMessage(task.getException().getMessage());
                }
            }
        });
    }

    private void RegistrarFerramentasDefesa(Integer firewall, Integer antivpn, Integer criptografia,
                                           Integer antiransomware, Integer mitmprotection, Integer ddosprotection,
                                           Integer antivirus, Integer antiphishing, Integer defaceprotection, Integer custo){
        ClasseFerramentasDefesa defesa = new ClasseFerramentasDefesa(
                firewall, antivpn, criptografia, antiransomware, mitmprotection,
                ddosprotection, antivirus, antiphishing, defaceprotection, custo
        );

        FirebaseDatabase.getInstance().getReference("FerramentasDefesa")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .setValue(defesa).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                }else{
                    showMessage(task.getException().getMessage());
                }
            }
        });
    }

    private void RegistrarPerfil(Integer ativarbot, Integer fazerdeface, Integer concluirdesafios, Integer desenvolvedor,
                                Integer darexploit, Integer encerrarcontratos, Integer fazermitm, Integer fazerphishing,
                                Integer fazerddos, Integer serprocurado, Integer fazerransomware, Integer limparrastros,
                                Integer recrutar, Integer fazerdecrypt, Integer roubar, Integer terminartrabalho,
                                Integer participareventos){
        ClassePerfil perfil = new ClassePerfil(ativarbot, fazerdeface, concluirdesafios, desenvolvedor, darexploit,
                    encerrarcontratos, fazermitm, fazerphishing, fazerddos, serprocurado, fazerransomware,
                    limparrastros, recrutar, fazerdecrypt, roubar, terminartrabalho, participareventos
        );

        FirebaseDatabase.getInstance().getReference("Perfil")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .setValue(perfil).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                }else{
                    showMessage(task.getException().getMessage());
                }
            }
        });
    }

    private void RegistrarDados(){
        Integer exploit = 1; Integer vpn = 1; Integer decrypt = 1; Integer ransomware = 1; Integer mitm = 1;
        Integer ddos = 1; Integer botnet = 1; Integer phishing = 1; Integer deface = 1; Integer custoa = 500;

        Integer firewall = 1; Integer antivpn = 1; Integer criptografia = 1; Integer antiransomware = 1;
        Integer mitmprotection = 1; Integer ddosprotection = 1; Integer antivirus = 1; Integer antiphishing = 1;
        Integer defaceprotection = 1; Integer custod = 500;

        Integer ativarbot = 0; Integer fazerdeface = 0; Integer concluirdesafios = 0; Integer desenvolvedor = 0;
        Integer darexploit = 0; Integer encerrarcontratos = 0; Integer fazermitm = 0; Integer fazerphishing = 0;
        Integer fazerddos = 0; Integer serprocurado = 0; Integer fazerransomware = 0; Integer limparrastros = 0;
        Integer recrutar = 0; Integer fazerdecrypt = 0; Integer roubar = 0; Integer terminartrabalho = 0;
        Integer participareventos = 0;

        RegistrarFerramentasAtaque(exploit, vpn, decrypt, ransomware, mitm, ddos, botnet, phishing, deface, custoa);

        RegistrarFerramentasDefesa(firewall, antivpn, criptografia, antiransomware, mitmprotection, ddosprotection,
                antivirus, antiphishing, defaceprotection, custod);

        RegistrarPerfil(ativarbot, fazerdeface, concluirdesafios, desenvolvedor, darexploit, encerrarcontratos,
                fazermitm, fazerphishing, fazerddos, serprocurado, fazerransomware, limparrastros, recrutar, fazerdecrypt,
                roubar, terminartrabalho, participareventos);
    }

    private String criarEnderecoIP() {
        Random r = new Random();
        return r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256);
    }

    private String criarCarteira(int lenght){
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWYXZ0123456789".toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < lenght; i++){
            char c = chars[random.nextInt(chars.length)];
            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }

    private void Registrado() {
        Intent InicioActivity = new Intent(getApplicationContext(), EscolherClasseActivity.class);
        startActivity(InicioActivity);
        finish();
    }

    private void showMessage(String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.reg_btn:
                reg_btn.setVisibility(View.INVISIBLE);
                reg_progressbar.setVisibility(View.VISIBLE);

                final String email = reg_email.getText().toString();
                final String usuario = reg_usuario.getText().toString();
                final String senha = reg_senha.getText().toString();
                final String confirmar = reg_confirmar.getText().toString();

                if ( usuario.isEmpty() || email.isEmpty() || senha.isEmpty() || !senha.equals(confirmar)) {
                    showMessage("Verifique os campos preenchidos");
                    reg_btn.setVisibility(View.VISIBLE);
                    reg_progressbar.setVisibility(View.INVISIBLE);
                } else {
                    if (Functions.verificarInternet(this)) {
                        CriarContadeUsuario(email, usuario, senha);
                    } else {
                        Toast.makeText(getBaseContext(), "Verifique sua conexão de internet", Toast.LENGTH_LONG).show();
                    }
                }
            break;

            case R.id.label_login:
                startActivity(new Intent(this, LoginActivity.class));
            break;
        }
    }
}