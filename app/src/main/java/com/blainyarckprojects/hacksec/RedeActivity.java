package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RedeActivity extends AppCompatActivity {
    private Button btn_scan, btn_exploit;
    private EditText input_endereco_alvo;
    private TextView text_rede_ip;
    private ImageView btn_exploits;
    private DatabaseReference mDatabase;

    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rede);

        btn_scan = findViewById(R.id.btn_scan);
        input_endereco_alvo = findViewById(R.id.input_endereco_alvo);
        text_rede_ip = findViewById(R.id.text_rede_ip);
        btn_exploit = findViewById(R.id.btn_exploit);
        btn_exploits = findViewById(R.id.btn_exploits);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layoutexploit);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mDatabase = FirebaseDatabase.getInstance().getReference("Users");

        btn_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        String ip = dataSnapshot.child("enderecoip").getValue().toString();
                        String alvo = input_endereco_alvo.getText().toString();

                        if (alvo.equals(ip)){
                            text_rede_ip.setText(alvo);
                        }
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        btn_exploit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        String ip = dataSnapshot.child("enderecoip").getValue().toString();
                        String alvo = input_endereco_alvo.getText().toString();

                        if (alvo.equals(ip)){
                            Intent intent = new Intent(RedeActivity.this, AlvoActivity.class);
                            intent.putExtra("ip_alvo", alvo);
                            startActivityForResult(intent, 1);
                        }
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        btn_exploits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                myDialog.show();
            }
        });
    }
}
