package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AlvoActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView label_ip, label_usuario, label_grupo_hacker, label_level, label_conexao;
    private ImageView btn_ataque_ransomware, btn_ataque_ddos, btn_ataque_mitm,
            btn_ataque_phishing, btn_ataque_botnet, btn_ataque_deface, btn_ataque_carteira, icone_classe;
    DatabaseReference mDatabase;

    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alvo);

        //Intent intent = getIntent();
        //String ip_alvo = intent.getStringExtra("ip_alvo");

        label_ip = findViewById(R.id.label_ip);
        label_usuario = findViewById(R.id.label_usuario);
        label_grupo_hacker = findViewById(R.id.label_grupo_hacker);
        label_level = findViewById(R.id.label_level);
        label_conexao = findViewById(R.id.label_conexao);

        icone_classe = findViewById(R.id.img_classe);

        btn_ataque_ransomware = findViewById(R.id.btn_ataque_ransomware);
        btn_ataque_ddos = findViewById(R.id.btn_ataque_ddos);
        btn_ataque_mitm = findViewById(R.id.btn_ataque_mitm);
        btn_ataque_phishing = findViewById(R.id.btn_ataque_phishing);
        btn_ataque_botnet = findViewById(R.id.btn_ataque_botnet);
        btn_ataque_deface = findViewById(R.id.btn_ataque_deface);
        btn_ataque_carteira = findViewById(R.id.btn_ataque_carteira);

        //label_ip.setText(ip_alvo);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layoutataque);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        btn_ataque_ransomware.setOnClickListener(this);
        btn_ataque_ddos.setOnClickListener(this);
        btn_ataque_mitm.setOnClickListener(this);
        btn_ataque_phishing.setOnClickListener(this);
        btn_ataque_botnet.setOnClickListener(this);
        btn_ataque_deface.setOnClickListener(this);
        btn_ataque_carteira.setOnClickListener(this);

        buscarAlvo();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_ataque_ransomware:
                    ataqueRansomware();
                break;
            case R.id.btn_ataque_ddos:
                    ataqueDdos();
                break;
            case R.id.btn_ataque_mitm:
                    ataqueMitm();
                break;
            case R.id.btn_ataque_phishing:
                    ataquePhishing();
                break;
            case R.id.btn_ataque_botnet:
                    ataqueBotnet();
                break;
            case R.id.btn_ataque_deface:
                    ataqueDeface();
                break;
            case R.id.btn_ataque_carteira:
                    ataqueCarteira();
                break;
        }
    }

    private void ataqueCarteira() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        TextView ferramenta = myDialog.findViewById(R.id.label_titulo);
        ImageView img_principal = myDialog.findViewById(R.id.img_principal);
        TextView descricao = myDialog.findViewById(R.id.label_descricao);

        descricao.setText("Inicie a descriptografia para saquear a Carteira do dispositivo.");
        img_principal.setImageResource(R.drawable.icone_carteira_layout);

        ferramenta.setText("Carteira");

        myDialog.show();
    }

    private void ataqueDeface() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        TextView ferramenta = myDialog.findViewById(R.id.label_titulo);
        ImageView img_principal = myDialog.findViewById(R.id.img_principal);
        TextView descricao = myDialog.findViewById(R.id.label_descricao);

        descricao.setText("Inicie o ataque Deface para desfigurar o sistema do dispositivo");
        img_principal.setImageResource(R.drawable.icone_deface_layout);

        ferramenta.setText("Deface");

        myDialog.show();
    }

    private void ataqueBotnet() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        TextView ferramenta = myDialog.findViewById(R.id.label_titulo);
        ImageView img_principal = myDialog.findViewById(R.id.img_principal);
        TextView descricao = myDialog.findViewById(R.id.label_descricao);

        descricao.setText("Ative o Botnet para criar sua rede de BOTs.");
        img_principal.setImageResource(R.drawable.icone_botnet_layout);

        ferramenta.setText("Botnet");

        myDialog.show();
    }

    private void ataquePhishing() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        TextView ferramenta = myDialog.findViewById(R.id.label_titulo);
        ImageView img_principal = myDialog.findViewById(R.id.img_principal);
        TextView descricao = myDialog.findViewById(R.id.label_descricao);

        descricao.setText("Inicie o ataque Phishing para capturar informações do dispositivo.");
        img_principal.setImageResource(R.drawable.icone_phishing_layout);

        ferramenta.setText("Phishing");

        myDialog.show();
    }

    private void ataqueMitm() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        TextView ferramenta = myDialog.findViewById(R.id.label_titulo);
        ImageView img_principal = myDialog.findViewById(R.id.img_principal);
        TextView descricao = myDialog.findViewById(R.id.label_descricao);

        descricao.setText("Inicie o ataque MITM para roubar dados de transações do dispositivo.");
        img_principal.setImageResource(R.drawable.icone_mitm_layout);

        ferramenta.setText("MITM");

        myDialog.show();
    }

    private void ataqueDdos() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        TextView ferramenta = myDialog.findViewById(R.id.label_titulo);
        ImageView img_principal = myDialog.findViewById(R.id.img_principal);
        TextView descricao = myDialog.findViewById(R.id.label_descricao);

        descricao.setText("Inicie o ataque DDoS para derrubar o sistema do dispositivo conectado.");
        img_principal.setImageResource(R.drawable.icone_ddos_layout);

        ferramenta.setText("DDoS");

        myDialog.show();
    }

    private void ataqueRansomware() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        TextView ferramenta = myDialog.findViewById(R.id.label_titulo);
        ImageView img_principal = myDialog.findViewById(R.id.img_principal);
        TextView descricao = myDialog.findViewById(R.id.label_descricao);

        descricao.setText("Inicie o ataque Ransomware para sequestrar os dados do dispositivo.");
        img_principal.setImageResource(R.drawable.icone_ransomware_layout);
        ferramenta.setText("Ransomware");

        myDialog.show();
    }

    private void buscarAlvo(){
        mDatabase = FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String enderecoip = dataSnapshot.child("enderecoip").getValue().toString();
                    String usuario = dataSnapshot.child("usuario").getValue().toString();
                    String level = dataSnapshot.child("level").getValue().toString();
                    String grupohacker = dataSnapshot.child("grupohacker").getValue().toString();
                    String conexao = dataSnapshot.child("conexao").getValue().toString();
                    String classe = dataSnapshot.child("classe").getValue().toString();

                    label_usuario.setText(usuario);
                    label_level.setText(level);
                    label_grupo_hacker.setText(grupohacker);
                    label_ip.setText(enderecoip);
                    label_conexao.setText(conexao);

                    if (classe.equals("WhiteHat")) {
                        icone_classe.setImageResource(R.drawable.icone_whitehat);
                    } else if (classe.equals("GreyHat")) {
                        icone_classe.setImageResource(R.drawable.icone_greyhat);
                    } else {
                        icone_classe.setImageResource(R.drawable.icone_blackhat);
                    }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
