package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GrupoHackerActivity extends AppCompatActivity {

    private Button btn_grupo_hacker_ferramentas, btn_grupo_hacker_comunicacao;
    private ListView listview_grupohacker;
    private TextView label_nome, label_tag, label_fundador, label_reputacao,
            label_ranking, label_membros;
    DatabaseReference mDatabase;

    String[] list_usuario = {"BlainyArck", "Pyrorubro", "Samurai", "NegoPreto", "xDuduAlp", "einK", "Ceifador",
            "plpfantasma", "CahAbreu", "BrunoLokao", "RaptorCyrax", " ViniciusHSA", "jefeeh", "Stark171", "Ghost", "fx61080TI",
            "fANCYkING", "Deltak", "PRED", "iDiniom"};
    String[] list_level = {"1", "1", "1", "1", "1", "1", "1", "1", "1", "1",
            "1", "1", "1", "1", "1", "1", "1", "1", "1", "1"};
    String[] list_rep = {"90", "90", "90", "90", "90", "90", "90", "90", "90", "90",
            "90", "90", "90", "90", "90", "90", "90", "90", "90", "90"};
    String[] list_cargo = {"Fundador", "Co-Fundador", "Membro", "Membro", "Membro", "Membro", "Membro", "Membro", "Membro", "Membro", "Membro", "Membro", "Membro",
            "Membro", "Membro", "Membro", "Membro", "Membro", "Membro", "Membro"};

    int [] imgs = {R.drawable.icone_whitehat, R.drawable.icone_greyhat, R.drawable.icone_blackhat,
            R.drawable.icone_whitehat, R.drawable.icone_greyhat, R.drawable.icone_blackhat,
            R.drawable.icone_whitehat, R.drawable.icone_greyhat, R.drawable.icone_blackhat,
            R.drawable.icone_whitehat, R.drawable.icone_whitehat, R.drawable.icone_greyhat,
            R.drawable.icone_blackhat, R.drawable.icone_whitehat, R.drawable.icone_greyhat,
            R.drawable.icone_blackhat, R.drawable.icone_whitehat, R.drawable.icone_greyhat,
            R.drawable.icone_blackhat, R.drawable.icone_whitehat};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupo_hacker);

        btn_grupo_hacker_ferramentas = findViewById(R.id.btn_gh_ferramentas);
        btn_grupo_hacker_comunicacao = findViewById(R.id.btn_comunicacao);
        label_nome = findViewById(R.id.label_titulo);
        label_tag = findViewById(R.id.label_tag);
        label_fundador = findViewById(R.id.label_fundador);
        label_reputacao = findViewById(R.id.label_reputacao);
        label_ranking = findViewById(R.id.label_ranking);
        label_membros = findViewById(R.id.label_membros);
        listview_grupohacker = findViewById(R.id.listview_grupohacker);

        buscarGrupoHacker();

        btn_grupo_hacker_ferramentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GrupoHackerActivity.this,GrupoHackerFerramentasActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_grupo_hacker_comunicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GrupoHackerActivity.this,GrupoHackerComunicacaoActivity.class);
                startActivity(intent);
                finish();
            }
        });

        MyAdapter adapter = new MyAdapter(this, imgs, list_usuario, list_level, list_rep, list_cargo);

        listview_grupohacker.setAdapter(adapter);

    }

    public void buscarGrupoHacker(){
        mDatabase = FirebaseDatabase.getInstance().getReference("GrupoHacker")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String nome = dataSnapshot.child("nome").getValue().toString();
                String tag = dataSnapshot.child("tag").getValue().toString();
                String fundador = dataSnapshot.child("fundador").getValue().toString();
                String ranking = dataSnapshot.child("ranking").getValue().toString();
                String membros = dataSnapshot.child("membros").getValue().toString();
                String reputacao = dataSnapshot.child("reputacao").getValue().toString();

                label_nome.setText(nome);
                label_tag.setText(tag);
                label_fundador.setText(fundador);
                label_ranking.setText(ranking);
                label_membros.setText(membros);
                label_reputacao.setText(reputacao);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    class MyAdapter extends ArrayAdapter<String>{
        Context context;
        int[] imgs;
        String list_usuario[];
        String list_level[];
        String list_rep[];
        String list_cargo[];


        MyAdapter(Context c, int[] imgs, String[] usuario, String[] level, String[] rep, String[] cargo){
            super(c, R.layout.listview_procurados, R.id.label_usuario, usuario);
            this.context = c;
            this.imgs = imgs;
            this.list_usuario = usuario;
            this.list_level = level;
            this.list_rep = rep;
            this.list_cargo = cargo;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_grupohacker, parent, false);

            ImageView images = row.findViewById(R.id.img);
            TextView usuario = row.findViewById(R.id.label_usuario);
            TextView level = row.findViewById(R.id.label_level);
            TextView rep = row.findViewById(R.id.label_rep);
            TextView cargo = row.findViewById(R.id.label_cargo);

            images.setImageResource(imgs[position]);
            usuario.setText(list_usuario[position]);
            level.setText(list_level[position]);
            rep.setText(list_rep[position]);
            cargo.setText(list_cargo[position]);
            return row;
        }
    }
}
