package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class RastrosActivity extends AppCompatActivity {

    private ImageView btn_rastros_transacoes;
    ListView listview_rastros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rastros);

        btn_rastros_transacoes = findViewById(R.id.btn_rastros_transacoes);
        listview_rastros = findViewById(R.id.listview_rastros);

        String list_rastros[] = {"[04/13 18:53] 192.168.16.16 acessou seu dispositivo",
        "[04/13 18:36] 192.168.16.16 acessou seu dispositivo", "[04/13 18:30] 192.168.16.16 acessou seu dispositivo"};

        MyAdapter adapter = new MyAdapter(this, list_rastros);

        listview_rastros.setAdapter(adapter);

        btn_rastros_transacoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RastrosActivity.this, TransacoesActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        String list_rastros[];

        MyAdapter(Context c, String[] rastros){
            super(c, R.layout.listview_rastros, R.id.label_rastros, rastros);
            this.context = c;
            this.list_rastros = rastros;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_rastros, parent, false);

            TextView rastros = row.findViewById(R.id.label_rastros);

            rastros.setText(list_rastros[position]);
            return row;
        }
    }
}
