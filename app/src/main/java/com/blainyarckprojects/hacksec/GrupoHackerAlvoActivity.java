package com.blainyarckprojects.hacksec;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GrupoHackerAlvoActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_grupo_hacker, btn_comunicacao, btn_gh_ferramentas, btn_updates, btn_ferramentas;
    private TextView label_nome, label_tag, label_fundador, label_reputacao,
            label_ranking, label_membros;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupo_hacker_alvo);

        btn_grupo_hacker = findViewById(R.id.btn_grupo_hacker);
        btn_comunicacao = findViewById(R.id.btn_comunicacao);
        btn_gh_ferramentas = findViewById(R.id.btn_gh_ferramentas);

        btn_updates = findViewById(R.id.btn_updates);
        btn_ferramentas = findViewById(R.id.btn_ferramentas);

        label_nome = findViewById(R.id.label_titulo);
        label_tag = findViewById(R.id.label_tag);
        label_fundador = findViewById(R.id.label_fundador);
        label_reputacao = findViewById(R.id.label_reputacao);
        label_ranking = findViewById(R.id.label_ranking);
        label_membros = findViewById(R.id.label_membros);

        btn_grupo_hacker.setOnClickListener(this);
        btn_comunicacao.setOnClickListener(this);
        btn_gh_ferramentas.setOnClickListener(this);

        btn_updates.setOnClickListener(this);
        btn_ferramentas.setOnClickListener(this);

        buscarGrupoHacker();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_grupo_hacker:
                startActivity(new Intent(this, GrupoHackerActivity.class));
                finish();
                break;
            case R.id.btn_comunicacao:
                startActivity(new Intent(this, GrupoHackerComunicacaoActivity.class));
                finish();
                break;
            case R.id.btn_gh_ferramentas:
                startActivity(new Intent(this, GrupoHackerActivity.class));
                finish();
                break;
            case R.id.btn_updates:
                startActivity(new Intent(this, GrupoHackerUpdatesActivity.class));
                finish();
                break;
            case R.id.btn_ferramentas:
                startActivity(new Intent(this, GrupoHackerFerramentasActivity.class));
                finish();
                break;
        }
    }

    public void buscarGrupoHacker(){
        mDatabase = FirebaseDatabase.getInstance().getReference("GrupoHacker")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String nome = dataSnapshot.child("nome").getValue().toString();
                String tag = dataSnapshot.child("tag").getValue().toString();
                String fundador = dataSnapshot.child("fundador").getValue().toString();
                String ranking = dataSnapshot.child("ranking").getValue().toString();
                String membros = dataSnapshot.child("membros").getValue().toString();
                String reputacao = dataSnapshot.child("reputacao").getValue().toString();

                label_nome.setText(nome);
                label_tag.setText(tag);
                label_fundador.setText(fundador);
                label_ranking.setText(ranking);
                label_membros.setText(membros);
                label_reputacao.setText(reputacao);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
