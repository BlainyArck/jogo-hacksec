package com.blainyarckprojects.hacksec;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.blainyarckprojects.hacksec.classes.ClasseCriarGrupoHacker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class CriarGrupoHackerActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_grupo_hacker_entrar, btn_grupo_hacker_criar;
    private EditText input_criar_nome, input_criar_tag;
    private DatabaseReference mDatabase;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar_grupo_hacker);

        btn_grupo_hacker_entrar = findViewById(R.id.btn_grupo_hacker_entrar);
        btn_grupo_hacker_criar = findViewById(R.id.btn_grupo_hacker_criar);
        input_criar_nome = findViewById(R.id.input_criar_nome);
        input_criar_tag = findViewById(R.id.input_criar_tag);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);

        mDatabase = FirebaseDatabase.getInstance().getReference("Users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        btn_grupo_hacker_entrar.setOnClickListener(this);
        btn_grupo_hacker_criar.setOnClickListener(this);

    }

    private void showMessage(String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }

    private void Registrado() {
        startActivity(new Intent(this, GrupoHackerActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_grupo_hacker_entrar:
                showMessage("Está funcionalidade ainda não está pronta.");
            break;
            case R.id.btn_grupo_hacker_criar:
                criarGP();
            break;
        }
    }

    private void criarGP() {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.VISIBLE);
                btn_grupo_hacker_criar.setVisibility(View.INVISIBLE);

                final String nome = input_criar_nome.getText().toString();
                final String tag = input_criar_tag.getText().toString();
                final String fundador = dataSnapshot.child("usuario").getValue().toString();
                final Integer membros = 1;
                final String reputacao = dataSnapshot.child("reputacao").getValue().toString();
                Integer reputacaoi = Integer.parseInt(reputacao);
                final Integer ranking = 1;
                final String cargo = "Fundador";

                dataSnapshot.getRef().child("grupohacker").setValue(input_criar_nome.getText().toString());
                dataSnapshot.getRef().child("cargo").setValue(cargo).toString();

                CriarGrupoHacker(nome, tag, fundador, membros, reputacaoi, ranking);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showMessage("Não foi possível criar o Grupo Hacker.");
            }
        });
    }

    private void CriarGrupoHacker (String nome, String tag, String fundador, Integer membros,
                                   Integer reputacao, Integer ranking){

        ClasseCriarGrupoHacker grupohacker = new ClasseCriarGrupoHacker(
                nome, tag, fundador, membros, reputacao, ranking
        );

        FirebaseDatabase.getInstance().getReference("GrupoHacker").child(FirebaseAuth.getInstance()
                .getCurrentUser().getUid()).setValue(grupohacker).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    showMessage("Grupo Hacker criado com sucesso!");
                    Registrado();
                }else{
                    showMessage("Erro ao tentar criar." + task.getException().getMessage());
                }
            }
        });
    }
}
