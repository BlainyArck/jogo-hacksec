package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ServerActivity extends AppCompatActivity {

    private ImageView btn_server_sniffer, btn_botnet1, btn_botnet2, btn_botnet3,
            btn_botnet4, btn_botnet5, btn_botnet6, btn_botnet7, btn_botnet8, btn_botnet9,
            btn_heist, btn_bankattack, btn_conexoes;
    private Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);

        btn_server_sniffer = findViewById(R.id.btn_server_sniffer);
        btn_heist = findViewById(R.id.btn_heist);
        btn_bankattack = findViewById(R.id.btn_bankattack);
        btn_conexoes = findViewById(R.id.btn_conexoes);

        btn_botnet1 = findViewById(R.id.btn_botnet1);
        btn_botnet2 = findViewById(R.id.btn_botnet2);
        btn_botnet3 = findViewById(R.id.btn_botnet3);
        btn_botnet4 = findViewById(R.id.btn_botnet4);
        btn_botnet5 = findViewById(R.id.btn_botnet5);
        btn_botnet6 = findViewById(R.id.btn_botnet6);
        btn_botnet7 = findViewById(R.id.btn_botnet7);
        btn_botnet8 = findViewById(R.id.btn_botnet8);
        btn_botnet9 = findViewById(R.id.btn_botnet9);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layoutbotnet);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        btn_conexoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ServerActivity.this, ConexoesActivity.class));
            }
        });

        btn_server_sniffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServerActivity.this, SnifferActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_heist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServerActivity.this, HeistActivity.class);
                startActivity(intent);
            }
        });

        btn_bankattack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServerActivity.this, BankAttackActivity.class);
                startActivity(intent);
            }
        });

        btn_botnet1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView ddos = myDialog.findViewById(R.id.label_ddos);
                TextView exploit = myDialog.findViewById(R.id.label_exploit);
                TextView decrypt = myDialog.findViewById(R.id.label_decrypt);

                ddos.setText("0");
                exploit.setText("0");
                decrypt.setText("0");

                myDialog.show();
            }
        });

        btn_botnet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView ddos = myDialog.findViewById(R.id.label_ddos);
                TextView exploit = myDialog.findViewById(R.id.label_exploit);
                TextView decrypt = myDialog.findViewById(R.id.label_decrypt);

                ddos.setText("0");
                exploit.setText("0");
                decrypt.setText("0");

                myDialog.show();
            }
        });

        btn_botnet3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView ddos = myDialog.findViewById(R.id.label_ddos);
                TextView exploit = myDialog.findViewById(R.id.label_exploit);
                TextView decrypt = myDialog.findViewById(R.id.label_decrypt);

                ddos.setText("0");
                exploit.setText("0");
                decrypt.setText("0");

                myDialog.show();
            }
        });

        btn_botnet4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView ddos = myDialog.findViewById(R.id.label_ddos);
                TextView deface = myDialog.findViewById(R.id.label_exploit);
                TextView decrypt = myDialog.findViewById(R.id.label_decrypt);

                ddos.setText("0");
                deface.setText("0");
                decrypt.setText("0");

                myDialog.show();
            }
        });

        btn_botnet5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView ddos = myDialog.findViewById(R.id.label_ddos);
                TextView exploit = myDialog.findViewById(R.id.label_exploit);
                TextView decrypt = myDialog.findViewById(R.id.label_decrypt);

                ddos.setText("0");
                exploit.setText("0");
                decrypt.setText("0");

                myDialog.show();
            }
        });

        btn_botnet6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView ddos = myDialog.findViewById(R.id.label_ddos);
                TextView exploit = myDialog.findViewById(R.id.label_exploit);
                TextView decrypt = myDialog.findViewById(R.id.label_decrypt);

                ddos.setText("0");
                exploit.setText("0");
                decrypt.setText("0");

                myDialog.show();
            }
        });

        btn_botnet7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView ddos = myDialog.findViewById(R.id.label_ddos);
                TextView exploit = myDialog.findViewById(R.id.label_exploit);
                TextView decrypt = myDialog.findViewById(R.id.label_decrypt);

                ddos.setText("0");
                exploit.setText("0");
                decrypt.setText("0");

                myDialog.show();
            }
        });

        btn_botnet8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView ddos = myDialog.findViewById(R.id.label_ddos);
                TextView exploit = myDialog.findViewById(R.id.label_exploit);
                TextView decrypt = myDialog.findViewById(R.id.label_decrypt);

                ddos.setText("0");
                exploit.setText("0");
                decrypt.setText("0");

                myDialog.show();
            }
        });

        btn_botnet9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView ddos = myDialog.findViewById(R.id.label_ddos);
                TextView exploit = myDialog.findViewById(R.id.label_exploit);
                TextView decrypt = myDialog.findViewById(R.id.label_decrypt);

                ddos.setText("0");
                exploit.setText("0");
                decrypt.setText("0");

                myDialog.show();
            }
        });
    }
}
