package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ConexoesActivity extends AppCompatActivity {

    private ListView listview_conexoes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conexoes);

        listview_conexoes = findViewById(R.id.listview_conexoes);

        int[] list_img = {R.drawable.icone_whitehat, R.drawable.icone_blackhat};
        String[] list_usuario = {"Pyrorubro", "NegoPreto"};
        String[] list_ips = {"192.168.15.60", "117.237.2.64"};

        MyAdapter adapter = new MyAdapter(this, list_img, list_usuario, list_ips);

        listview_conexoes.setAdapter(adapter);
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        int[] imgs;
        String list_usuario[];
        String list_ips[];


        MyAdapter(Context c, int[] imgs, String[] usuario, String[] ips){
            super(c, R.layout.listview_procurados, R.id.conex_usuario, usuario);
            this.context = c;
            this.imgs = imgs;
            this.list_usuario = usuario;
            this.list_ips = ips;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_conexoes, parent, false);

            ImageView images = row.findViewById(R.id.conex_img);
            TextView usuario = row.findViewById(R.id.conex_usuario);
            TextView ips = row.findViewById(R.id.conex_ip);

            images.setImageResource(imgs[position]);
            usuario.setText(list_usuario[position]);
            ips.setText(list_ips[position]);
            return row;
        }
    }
}
