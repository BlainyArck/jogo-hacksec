package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class EventoTorneioActivity extends AppCompatActivity {
    private ListView listview_torneio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento_torneio);

        listview_torneio = findViewById(R.id.listview_torneio);

        String[] list_usuario = {"BlainyArck", "Pyrorubro", "Samurai", "NegoPreto", "xDuduAlp", "einK", "Ceifador",
                "plpfantasma", "CahAbreu", "BrunoLokao", "RaptorCyrax", " ViniciusHSA", "jefeeh", "Stark171", "Ghost", "fx61080TI",
                "fANCYkING", "Deltak", "PRED", "iDiniom"};
        String[] list_level = {"1", "1", "1", "1", "1", "1", "1", "1", "1", "1",
                "1", "1", "1", "1", "1", "1", "1", "1", "1", "1"};
        String[] list_rep = {"90", "90", "90", "90", "90", "90", "90", "90", "90", "90",
                "90", "90", "90", "90", "90", "90", "90", "90", "90", "90"};
        String[] list_ranking = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"};

        int [] imgs = {R.drawable.icone_whitehat, R.drawable.icone_greyhat, R.drawable.icone_blackhat,
                R.drawable.icone_whitehat, R.drawable.icone_greyhat, R.drawable.icone_blackhat,
                R.drawable.icone_whitehat, R.drawable.icone_greyhat, R.drawable.icone_blackhat,
                R.drawable.icone_whitehat, R.drawable.icone_whitehat, R.drawable.icone_greyhat,
                R.drawable.icone_blackhat, R.drawable.icone_whitehat, R.drawable.icone_greyhat,
                R.drawable.icone_blackhat, R.drawable.icone_whitehat, R.drawable.icone_greyhat,
                R.drawable.icone_blackhat, R.drawable.icone_whitehat};

        MyAdapter adapter = new MyAdapter(this, imgs, list_usuario, list_level, list_rep, list_ranking);
        listview_torneio.setAdapter(adapter);
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        int[] imgs;
        String list_usuario[];
        String list_level[];
        String list_rep[];
        String list_ranking[];


        MyAdapter(Context c, int[] imgs, String[] usuario, String[] level, String[] rep, String[] ranking){
            super(c, R.layout.listview_procurados, R.id.label_usuario, usuario);
            this.context = c;
            this.imgs = imgs;
            this.list_usuario = usuario;
            this.list_level = level;
            this.list_rep = rep;
            this.list_ranking = ranking;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_procurados, parent, false);

            ImageView images = row.findViewById(R.id.img);
            TextView usuario = row.findViewById(R.id.label_usuario);
            TextView level = row.findViewById(R.id.label_level);
            TextView rep = row.findViewById(R.id.label_rep);
            TextView ranking = row.findViewById(R.id.label_ranking);

            images.setImageResource(imgs[position]);
            usuario.setText(list_usuario[position]);
            level.setText(list_level[position]);
            rep.setText(list_rep[position]);
            ranking.setText(list_ranking[position]);
            return row;
        }
    }
}
