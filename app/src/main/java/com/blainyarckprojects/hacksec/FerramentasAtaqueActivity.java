package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FerramentasAtaqueActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_ferramentas_defesa, btn_ferramentas_updates;
    private ImageView btn_exploit, btn_vpn, btn_decrypt, btn_ransomware,
            btn_mitm, btn_ddos, btn_botnet, btn_phishing, btn_deface;
    DatabaseReference mDatabase;

    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ferramentas_ataque);

        btn_ferramentas_defesa = findViewById(R.id.btn_ferramentas_defesa);
        btn_ferramentas_updates = findViewById(R.id.btn_ferramentas_updates);

        btn_exploit = findViewById(R.id.btn_exploit);
        btn_vpn = findViewById(R.id.btn_anti_vpn);
        btn_decrypt = findViewById(R.id.btn_decrypt);
        btn_ransomware = findViewById(R.id.btn_ransomware);
        btn_mitm = findViewById(R.id.btn_mitm);
        btn_ddos = findViewById(R.id.btn_ddos);
        btn_botnet = findViewById(R.id.btn_botnet);
        btn_phishing = findViewById(R.id.btn_phishing);
        btn_deface = findViewById(R.id.btn_deface);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layoutferramentas);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mDatabase = FirebaseDatabase.getInstance().getReference("FerramentasAtaque")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        btn_ferramentas_updates.setOnClickListener(this);
        btn_ferramentas_defesa.setOnClickListener(this);

        btn_exploit.setOnClickListener(this);
        btn_vpn.setOnClickListener(this);
        btn_decrypt.setOnClickListener(this);
        btn_ransomware.setOnClickListener(this);
        btn_mitm.setOnClickListener(this);
        btn_ddos.setOnClickListener(this);
        btn_botnet.setOnClickListener(this);
        btn_phishing.setOnClickListener(this);
        btn_deface.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_ferramentas_updates:
                startActivity(new Intent(this, FerramentasUpdatesActivity.class));
                finish();
            break;
            case R.id.btn_ferramentas_defesa:
                startActivity(new Intent(this, FerramentasDefesaActivity.class));
                finish();
            break;
            case R.id.btn_exploit:
                buttonExploit();
            break;
            case R.id.btn_anti_vpn:
                buttonAntiVpn();
            break;
            case R.id.btn_decrypt:
                buttonDecrypt();
            break;
            case R.id.btn_ransomware:
                buttonRansomware();
            break;
            case R.id.btn_mitm:
                buttonMitm();
            break;
            case R.id.btn_ddos:
                buttonDdos();
            break;
            case R.id.btn_botnet:
                buttonBotnet();
            break;
            case R.id.btn_phishing:
                buttonPhishing();
            break;
            case R.id.btn_deface:
                buttonDeface();
            break;
        }
    }

    private void buttonDeface() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("deface").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Deface");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_deface);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonPhishing() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("phishing").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Phishing");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_phishing);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonBotnet() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("botnet").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Botnet");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_botnet);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonDdos() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("ddos").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("DDoS");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_ddos);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonMitm() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("mitm").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("MITM");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_mitm);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonRansomware() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("ransomware").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Ransomware");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_ransomware);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonDecrypt() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("decrypt").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Decrypt");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_decrypt);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonAntiVpn() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("vpn").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("VPN");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_vpn);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonExploit() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("exploit").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Exploit");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_exploit);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }
}
