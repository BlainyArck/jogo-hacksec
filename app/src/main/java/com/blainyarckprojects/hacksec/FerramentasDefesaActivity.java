package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FerramentasDefesaActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_ferramentas_ataque, btn_ferramentas_updates;
    private ImageView btn_firewall, btn_anti_vpn, btn_criptografia, btn_anti_ransomware, btn_mitm_protection,
            btn_ddos_protection, btn_antivirus, btn_anti_phishing, btn_deface_protection;

    DatabaseReference mDatabase;
    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ferramentas_defesa);

        btn_ferramentas_ataque = findViewById(R.id.btn_ferramentas_ataque);
        btn_ferramentas_updates = findViewById(R.id.btn_ferramentas_updates);

        btn_firewall = findViewById(R.id.btn_firewall);
        btn_anti_vpn = findViewById(R.id.btn_anti_vpn);
        btn_criptografia = findViewById(R.id.btn_criptografia);
        btn_anti_ransomware = findViewById(R.id.btn_anti_ransomware);
        btn_mitm_protection = findViewById(R.id.btn_mitm_protection);
        btn_ddos_protection = findViewById(R.id.btn_ddos_protection);
        btn_antivirus = findViewById(R.id.btn_antivirus);
        btn_anti_phishing = findViewById(R.id.btn_anti_phishing);
        btn_deface_protection = findViewById(R.id.btn_deface_protection);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layoutferramentas);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mDatabase = FirebaseDatabase.getInstance().getReference("FerramentasDefesa")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        btn_ferramentas_updates.setOnClickListener(this);
        btn_ferramentas_ataque.setOnClickListener(this);

        btn_firewall.setOnClickListener(this);
        btn_anti_vpn.setOnClickListener(this);
        btn_criptografia.setOnClickListener(this);
        btn_anti_ransomware.setOnClickListener(this);
        btn_mitm_protection.setOnClickListener(this);
        btn_ddos_protection.setOnClickListener(this);
        btn_antivirus.setOnClickListener(this);
        btn_anti_phishing.setOnClickListener(this);
        btn_deface_protection.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_ferramentas_updates:
                startActivity(new Intent(this, FerramentasUpdatesActivity.class));
                finish();
            break;
            case R.id.btn_ferramentas_ataque:
                startActivity(new Intent(this, FerramentasAtaqueActivity.class));
                finish();
            break;
            case R.id.btn_firewall:
                buttonFirewall();
            break;
            case R.id.btn_anti_vpn:
                buttonVPN();
            break;
            case R.id.btn_criptografia:
                buttonCriptografia();
            break;
            case R.id.btn_anti_ransomware:
                buttonAntiRansomware();
            break;
            case R.id.btn_mitm_protection:
                buttonMitmProtection();
            break;
            case R.id.btn_ddos_protection:
                buttonDdosProtection();
            break;
            case R.id.btn_antivirus:
                buttonAntivirus();
            break;
            case R.id.btn_anti_phishing:
                buttonAntiPhishing();
            break;
            case R.id.btn_deface_protection:
                buttonDefaceProtection();
            break;
        }
    }

    private void buttonDefaceProtection() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("defaceprotection").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Deface Protection");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_antideface);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonAntiPhishing() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("antiphishing").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Anti-Phishing");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_antiphishing);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonAntivirus() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("antivirus").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Anti-Virus");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_antivirus);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonDdosProtection() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("ddosprotection").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("DDoS Protection");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_antiddos);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonMitmProtection() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("mitmprotection").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("MITM Protection");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_antimitm);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonAntiRansomware() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("antiransomware").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Anti-Ransomware");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_antiransomware);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonCriptografia() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("criptografia").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Criptografia");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_criptografia);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonVPN() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("antivpn").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Anti-VPN");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_antivpn);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }

    private void buttonFirewall() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("firewall").getValue().toString();
                String custo = dataSnapshot.child("custo").getValue().toString();

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Firewall");
                label_level_ferramenta.setText(level);
                label_custo_ferramenta.setText(custo);
                img.setImageResource(R.drawable.icone_firewall);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        myDialog.show();
    }
}
