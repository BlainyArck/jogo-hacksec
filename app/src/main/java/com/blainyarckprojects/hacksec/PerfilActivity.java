package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PerfilActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView btn_botnet, btn_deface, btn_desafios, btn_desenvolvedor, btn_exploit,
            btn_contratos, btn_level, btn_mitm, btn_phishing, btn_ddos, btn_procurados, btn_ransomware, btn_rastros,
            btn_recrutador, btn_decrypt, btn_roubos, btn_trabalho, btn_eventos, img_classe;

    private TextView text_reputacao, text_level;

    DatabaseReference mDatabase, usuario;

    Dialog myDialog, classeDialog;

    ListView listview_perfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        btn_botnet = findViewById(R.id.btn_botnet);
        btn_deface = findViewById(R.id.btn_deface);
        btn_desafios = findViewById(R.id.btn_desafios);
        btn_desenvolvedor = findViewById(R.id.btn_desenvolvedor);
        btn_exploit = findViewById(R.id.btn_exploits);
        btn_contratos = findViewById(R.id.btn_contratos);
        btn_level = findViewById(R.id.btn_level);
        btn_mitm = findViewById(R.id.btn_mitm);
        btn_phishing = findViewById(R.id.btn_phishing);
        btn_ddos = findViewById(R.id.btn_ddos);
        btn_procurados = findViewById(R.id.btn_procurados);
        btn_ransomware = findViewById(R.id.btn_ransomware);
        btn_rastros = findViewById(R.id.btn_rastros);
        btn_recrutador = findViewById(R.id.btn_recrutador);
        btn_decrypt = findViewById(R.id.btn_decrypt);
        btn_roubos = findViewById(R.id.btn_roubos);
        btn_trabalho = findViewById(R.id.btn_trabalho);
        btn_eventos = findViewById(R.id.btn_eventos);
        img_classe = findViewById(R.id.img);
        text_reputacao = findViewById(R.id.text_reputacao);
        text_level = findViewById(R.id.text_level);

        listview_perfil = findViewById(R.id.listview_perfil);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layoutmedalhas);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        classeDialog = new Dialog (this);
        classeDialog.setContentView(R.layout.layoutperfil);
        classeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mDatabase = FirebaseDatabase.getInstance().getReference("Perfil").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        usuario = FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        buscarUsuario();
        botnetMedalhas();
        defaceMedalhas();
        desafiosMedalhas();
        desenvolvedorMedalhas();
        exploitsMedalhas();
        contratosMedalhas();
        levelMedalhas();
        mitmMedalhas();
        phishingMedalhas();
        ddosMedalhas();
        procuradosMedalhas();
        ransomwareMedalhas();
        rastrosMedalhas();
        recrutadorMedalhas();
        decryptMedalhas();
        roubosMedalhas();
        trabalhoMedalhas();
        eventosMedalhas();

        img_classe.setOnClickListener(this);
        btn_botnet.setOnClickListener(this);
        btn_deface.setOnClickListener(this);
        btn_desafios.setOnClickListener(this);
        btn_desenvolvedor.setOnClickListener(this);
        btn_exploit.setOnClickListener(this);
        btn_contratos.setOnClickListener(this);
        btn_level.setOnClickListener(this);
        btn_mitm.setOnClickListener(this);
        btn_phishing.setOnClickListener(this);
        btn_ddos.setOnClickListener(this);
        btn_procurados.setOnClickListener(this);
        btn_ransomware.setOnClickListener(this);
        btn_rastros.setOnClickListener(this);
        btn_recrutador.setOnClickListener(this);
        btn_decrypt.setOnClickListener(this);
        btn_roubos.setOnClickListener(this);
        btn_trabalho.setOnClickListener(this);
        btn_eventos.setOnClickListener(this);

        String[] list_acoes = {"Ativar BotNet", "Fazer ataque Defacement", "Concluir desafios", "Desenvolver ferramentas",
                "Explorar dispositivos", "Concluir contratos", "Level", "Fazer ataque MITM", "Fazer ataque Phishing",
                "Fazer ataque DDoS", "Top Procurados", "Fazer ataque Ransomware", "Limpar Rastros", "Recrutar Hackers",
                "Descriptografar Carteira", "Realizar Roubos", "Concluir Trabalho", "Eventos Participados", };

        String[] list_realizados = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
                "0", "0", "0", "0", "0", "0", "0", "0", };

        MyAdapter adapter = new MyAdapter(this, list_acoes, list_realizados);

        listview_perfil.setAdapter(adapter);
    }

    private void buscarUsuario() {
        usuario.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String tipo_classe = dataSnapshot.child("classe").getValue().toString();
                String reputacao = dataSnapshot.child("reputacao").getValue().toString();
                // String level_req = dataSnapshot.child("level").getValue().toString();

                text_reputacao.setText(reputacao);

                if (tipo_classe.equals("WhiteHat")){
                    img_classe.setImageResource(R.drawable.icone_whitehat);
                }else if(tipo_classe.equals("GreyHat")){
                    img_classe.setImageResource(R.drawable.icone_greyhat);
                }else{
                    img_classe.setImageResource(R.drawable.icone_blackhat);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void botnetMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String botnet = dataSnapshot.child("ativarbot").getValue().toString();

                int intValor = Integer.parseInt(botnet);

                if (intValor < 1000){
                    btn_botnet.setImageResource(R.drawable.botnet_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_botnet.setImageResource(R.drawable.botnet_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_botnet.setImageResource(R.drawable.botnet_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_botnet.setImageResource(R.drawable.botnet_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_botnet.setImageResource(R.drawable.botnet_platina);
                } else if (intValor >= 5000){
                    btn_botnet.setImageResource(R.drawable.botnet_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void defaceMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String deface = dataSnapshot.child("fazerdeface").getValue().toString();

                int intValor = Integer.parseInt(deface);

                if (intValor < 1000){
                    btn_deface.setImageResource(R.drawable.deface_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_deface.setImageResource(R.drawable.deface_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_deface.setImageResource(R.drawable.deface_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_deface.setImageResource(R.drawable.deface_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_deface.setImageResource(R.drawable.deface_platina);
                } else if (intValor >= 5000){
                    btn_deface.setImageResource(R.drawable.deface_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void desafiosMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String desafios = dataSnapshot.child("concluirdesafios").getValue().toString();

                int intValor = Integer.parseInt(desafios);

                if (intValor < 1000){
                    btn_desafios.setImageResource(R.drawable.desafios_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_desafios.setImageResource(R.drawable.desafios_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_desafios.setImageResource(R.drawable.desafios_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_desafios.setImageResource(R.drawable.desafios_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_desafios.setImageResource(R.drawable.desafios_platina);
                } else if (intValor >= 5000){
                    btn_desafios.setImageResource(R.drawable.desafios_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void desenvolvedorMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String desenvolvedor = dataSnapshot.child("desenvolvedor").getValue().toString();

                int intValor = Integer.parseInt(desenvolvedor);

                if (intValor < 1000){
                    btn_desenvolvedor.setImageResource(R.drawable.desenvolvedor_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_desenvolvedor.setImageResource(R.drawable.desenvolvedor_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_desenvolvedor.setImageResource(R.drawable.desenvolvedor_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_desenvolvedor.setImageResource(R.drawable.desenvolvedor_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_desenvolvedor.setImageResource(R.drawable.desenvolvedor_platina);
                } else if (intValor >= 5000){
                    btn_desenvolvedor.setImageResource(R.drawable.desenvolvedor_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void exploitsMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String exploit = dataSnapshot.child("darexploit").getValue().toString();

                int intValor = Integer.parseInt(exploit);

                if (intValor < 1000){
                    btn_exploit.setImageResource(R.drawable.exploit_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_exploit.setImageResource(R.drawable.exploit_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_exploit.setImageResource(R.drawable.exploit_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_exploit.setImageResource(R.drawable.exploit_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_exploit.setImageResource(R.drawable.exploit_platina);
                } else if (intValor >= 5000){
                    btn_exploit.setImageResource(R.drawable.exploit_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void contratosMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String contratos = dataSnapshot.child("encerrarcontratos").getValue().toString();

                int intValor = Integer.parseInt(contratos);

                if (intValor < 1000){
                    btn_contratos.setImageResource(R.drawable.contratos_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_contratos.setImageResource(R.drawable.contratos_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_contratos.setImageResource(R.drawable.contratos_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_contratos.setImageResource(R.drawable.contratos_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_contratos.setImageResource(R.drawable.contratos_platina);
                } else if (intValor >= 5000){
                    btn_contratos.setImageResource(R.drawable.contratos_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void levelMedalhas(){
        usuario.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String level = dataSnapshot.child("level").getValue().toString();

                int intValor = Integer.parseInt(level);

                if (intValor < 10){
                    btn_level.setImageResource(R.drawable.level_block);
                } else if (intValor >= 10 && intValor < 20){
                    btn_level.setImageResource(R.drawable.level_bronze);
                } else if (intValor >= 20 && intValor < 30){
                    btn_level.setImageResource(R.drawable.level_prata);
                } else if (intValor >= 30 && intValor < 40){
                    btn_level.setImageResource(R.drawable.level_ouro);
                } else if (intValor >= 40 && intValor < 50){
                    btn_level.setImageResource(R.drawable.level_platina);
                } else if (intValor >= 50){
                    btn_level.setImageResource(R.drawable.level_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void mitmMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String mitm = dataSnapshot.child("fazermitm").getValue().toString();

                int intValor = Integer.parseInt(mitm);

                if (intValor < 1000){
                    btn_mitm.setImageResource(R.drawable.mitm_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_mitm.setImageResource(R.drawable.mitm_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_mitm.setImageResource(R.drawable.mitm_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_mitm.setImageResource(R.drawable.mitm_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_mitm.setImageResource(R.drawable.mitm_platina);
                } else if (intValor >= 5000){
                    btn_mitm.setImageResource(R.drawable.mitm_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void ddosMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String ddos = dataSnapshot.child("fazerddos").getValue().toString();

                int intValor = Integer.parseInt(ddos);

                if (intValor < 1000){
                    btn_ddos.setImageResource(R.drawable.ddos_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_ddos.setImageResource(R.drawable.ddos_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_ddos.setImageResource(R.drawable.ddos_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_ddos.setImageResource(R.drawable.ddos_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_ddos.setImageResource(R.drawable.ddos_platina);
                } else if (intValor >= 5000){
                    btn_ddos.setImageResource(R.drawable.ddos_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void procuradosMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String procurados = dataSnapshot.child("serprocurado").getValue().toString();

                int intValor = Integer.parseInt(procurados);

                if (intValor < 10){
                    btn_procurados.setImageResource(R.drawable.procurado_block);
                } else if (intValor >= 10 && intValor < 20){
                    btn_procurados.setImageResource(R.drawable.procurado_bronze);
                } else if (intValor >= 20 && intValor < 30){
                    btn_procurados.setImageResource(R.drawable.procurado_prata);
                } else if (intValor >= 30 && intValor < 40){
                    btn_procurados.setImageResource(R.drawable.procurado_ouro);
                } else if (intValor >= 40 && intValor < 50){
                    btn_procurados.setImageResource(R.drawable.procurado_platina);
                } else if (intValor >= 50){
                    btn_procurados.setImageResource(R.drawable.procurado_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void ransomwareMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String ransomware = dataSnapshot.child("fazerransomware").getValue().toString();

                int intValor = Integer.parseInt(ransomware);

                if (intValor < 1000){
                    btn_ransomware.setImageResource(R.drawable.ransomware_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_ransomware.setImageResource(R.drawable.ransomware_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_ransomware.setImageResource(R.drawable.ransomware_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_ransomware.setImageResource(R.drawable.ransomware_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_ransomware.setImageResource(R.drawable.ransomware_platina);
                } else if (intValor >= 5000){
                    btn_ransomware.setImageResource(R.drawable.ransomware_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void phishingMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String phishing = dataSnapshot.child("fazerphishing").getValue().toString();

                int intValor = Integer.parseInt(phishing);

                if (intValor < 1000){
                    btn_phishing.setImageResource(R.drawable.phishing_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_phishing.setImageResource(R.drawable.phishing_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_phishing.setImageResource(R.drawable.phishing_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_phishing.setImageResource(R.drawable.phishing_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_phishing.setImageResource(R.drawable.phishing_platina);
                } else if (intValor >= 5000){
                    btn_phishing.setImageResource(R.drawable.phishing_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void rastrosMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String rastros = dataSnapshot.child("limparrastros").getValue().toString();

                int intValor = Integer.parseInt(rastros);

                if (intValor < 1000){
                    btn_rastros.setImageResource(R.drawable.rastros_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_rastros.setImageResource(R.drawable.rastros_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_rastros.setImageResource(R.drawable.rastros_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_rastros.setImageResource(R.drawable.rastros_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_rastros.setImageResource(R.drawable.rastros_platina);
                } else if (intValor >= 5000){
                    btn_rastros.setImageResource(R.drawable.rastros_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void recrutadorMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String recrutador = dataSnapshot.child("recrutar").getValue().toString();

                int intValor = Integer.parseInt(recrutador);

                if (intValor < 10){
                    btn_recrutador.setImageResource(R.drawable.recrutador_block);
                } else if (intValor >= 10 && intValor < 20){
                    btn_recrutador.setImageResource(R.drawable.recrutador_bronze);
                } else if (intValor >= 20 && intValor < 30){
                    btn_recrutador.setImageResource(R.drawable.recrutador_prata);
                } else if (intValor >= 30 && intValor < 40){
                    btn_recrutador.setImageResource(R.drawable.recrutador_ouro);
                } else if (intValor >= 40 && intValor < 50){
                    btn_recrutador.setImageResource(R.drawable.recrutador_platina);
                } else if (intValor >= 50){
                    btn_recrutador.setImageResource(R.drawable.recrutador_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void decryptMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String decrypt = dataSnapshot.child("fazerdecrypt").getValue().toString();

                int intValor = Integer.parseInt(decrypt);

                if (intValor < 1000){
                    btn_decrypt.setImageResource(R.drawable.decrypt_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_decrypt.setImageResource(R.drawable.decrypt_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_decrypt.setImageResource(R.drawable.decrypt_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_decrypt.setImageResource(R.drawable.decrypt_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_decrypt.setImageResource(R.drawable.decrypt_platina);
                } else if (intValor >= 5000){
                    btn_decrypt.setImageResource(R.drawable.decrypt_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void roubosMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String roubos = dataSnapshot.child("roubar").getValue().toString();

                int intValor = Integer.parseInt(roubos);

                if (intValor < 1000){
                    btn_roubos.setImageResource(R.drawable.roubos_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_roubos.setImageResource(R.drawable.roubos_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_roubos.setImageResource(R.drawable.roubos_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_roubos.setImageResource(R.drawable.roubos_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_roubos.setImageResource(R.drawable.roubos_platina);
                } else if (intValor >= 5000){
                    btn_roubos.setImageResource(R.drawable.roubos_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void trabalhoMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String trabalho = dataSnapshot.child("terminartrabalho").getValue().toString();

                int intValor = Integer.parseInt(trabalho);

                if (intValor < 1000){
                    btn_trabalho.setImageResource(R.drawable.trabalho_block);
                } else if (intValor >= 1000 && intValor < 2000){
                    btn_trabalho.setImageResource(R.drawable.trabalho_bronze);
                } else if (intValor >= 2000 && intValor < 3000){
                    btn_trabalho.setImageResource(R.drawable.trabalho_prata);
                } else if (intValor >= 3000 && intValor < 4000){
                    btn_trabalho.setImageResource(R.drawable.trabalho_ouro);
                } else if (intValor >= 4000 && intValor < 5000){
                    btn_trabalho.setImageResource(R.drawable.trabalho_platina);
                } else if (intValor >= 5000){
                    btn_trabalho.setImageResource(R.drawable.trabalho_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void eventosMedalhas(){
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String eventos = dataSnapshot.child("participareventos").getValue().toString();

                int intValor = Integer.parseInt(eventos);

                if (intValor < 10){
                    btn_eventos.setImageResource(R.drawable.eventos_block);
                } else if (intValor >= 10 && intValor < 20){
                    btn_eventos.setImageResource(R.drawable.eventos_bronze);
                } else if (intValor >= 20 && intValor < 30){
                    btn_eventos.setImageResource(R.drawable.eventos_prata);
                } else if (intValor >= 30 && intValor < 40){
                    btn_eventos.setImageResource(R.drawable.eventos_ouro);
                } else if (intValor >= 40 && intValor < 50){
                    btn_eventos.setImageResource(R.drawable.eventos_platina);
                } else if (intValor >= 50){
                    btn_eventos.setImageResource(R.drawable.eventos_black);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img:
                usuario.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String usuario = dataSnapshot.child("usuario").getValue().toString();
                        String classeusuario = dataSnapshot.child("classe").getValue().toString();
                        String levelusuario = dataSnapshot.child("level").getValue().toString();
                        // String ranking
                        String grupohacker = dataSnapshot.child("grupohacker").getValue().toString();

                        ImageView classe_img = classeDialog.findViewById(R.id.classe_img);
                        TextView text_usuario = classeDialog.findViewById(R.id.text_usuario);
                        TextView text_classe = classeDialog.findViewById(R.id.text_classe);
                        TextView text_level = classeDialog.findViewById(R.id.text_level);
                        TextView text_grupohacker = classeDialog.findViewById(R.id.text_grupo_hacker);

                        if (classeusuario.equals("WhiteHat")){
                            classe_img.setImageResource(R.drawable.icone_whitehat);
                        }else if(classeusuario.equals("GreyHat")){
                            classe_img.setImageResource(R.drawable.icone_greyhat);
                        }else{
                            classe_img.setImageResource(R.drawable.icone_blackhat);
                        }

                        text_usuario.setText(usuario);
                        text_classe.setText(classeusuario);
                        text_level.setText(levelusuario);
                        text_grupohacker.setText(grupohacker);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                classeDialog.show();
            break;
            case R.id.btn_botnet:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("ativarbot").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Botnet");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.botnet_block);
                            img_bronze.setImageResource(R.drawable.botnet_block);
                            img_prata.setImageResource(R.drawable.botnet_block);
                            img_ouro.setImageResource(R.drawable.botnet_block);
                            img_platina.setImageResource(R.drawable.botnet_block);
                            img_black.setImageResource(R.drawable.botnet_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.botnet_bronze);
                            img_bronze.setImageResource(R.drawable.botnet_bronze);
                            img_prata.setImageResource(R.drawable.botnet_block);
                            img_ouro.setImageResource(R.drawable.botnet_block);
                            img_platina.setImageResource(R.drawable.botnet_block);
                            img_black.setImageResource(R.drawable.botnet_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.botnet_prata);
                            img_bronze.setImageResource(R.drawable.botnet_bronze);
                            img_prata.setImageResource(R.drawable.botnet_prata);
                            img_ouro.setImageResource(R.drawable.botnet_block);
                            img_platina.setImageResource(R.drawable.botnet_block);
                            img_black.setImageResource(R.drawable.botnet_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.botnet_ouro);
                            img_bronze.setImageResource(R.drawable.botnet_bronze);
                            img_prata.setImageResource(R.drawable.botnet_prata);
                            img_ouro.setImageResource(R.drawable.botnet_ouro);
                            img_platina.setImageResource(R.drawable.botnet_block);
                            img_black.setImageResource(R.drawable.botnet_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.botnet_platina);
                            img_bronze.setImageResource(R.drawable.botnet_bronze);
                            img_prata.setImageResource(R.drawable.botnet_prata);
                            img_ouro.setImageResource(R.drawable.botnet_ouro);
                            img_platina.setImageResource(R.drawable.botnet_platina);
                            img_black.setImageResource(R.drawable.botnet_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.botnet_black);
                            img_bronze.setImageResource(R.drawable.botnet_bronze);
                            img_prata.setImageResource(R.drawable.botnet_prata);
                            img_ouro.setImageResource(R.drawable.botnet_ouro);
                            img_platina.setImageResource(R.drawable.botnet_platina);
                            img_black.setImageResource(R.drawable.botnet_black);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_deface:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("fazerdeface").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Deface");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.deface_block);
                            img_bronze.setImageResource(R.drawable.deface_block);
                            img_prata.setImageResource(R.drawable.deface_block);
                            img_ouro.setImageResource(R.drawable.deface_block);
                            img_platina.setImageResource(R.drawable.deface_block);
                            img_black.setImageResource(R.drawable.deface_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.deface_bronze);
                            img_bronze.setImageResource(R.drawable.deface_bronze);
                            img_prata.setImageResource(R.drawable.deface_block);
                            img_ouro.setImageResource(R.drawable.deface_block);
                            img_platina.setImageResource(R.drawable.deface_block);
                            img_black.setImageResource(R.drawable.deface_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.deface_prata);
                            img_bronze.setImageResource(R.drawable.deface_bronze);
                            img_prata.setImageResource(R.drawable.deface_prata);
                            img_ouro.setImageResource(R.drawable.deface_block);
                            img_platina.setImageResource(R.drawable.deface_block);
                            img_black.setImageResource(R.drawable.deface_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.deface_ouro);
                            img_bronze.setImageResource(R.drawable.deface_bronze);
                            img_prata.setImageResource(R.drawable.deface_prata);
                            img_ouro.setImageResource(R.drawable.deface_ouro);
                            img_platina.setImageResource(R.drawable.deface_block);
                            img_black.setImageResource(R.drawable.deface_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.deface_platina);
                            img_bronze.setImageResource(R.drawable.deface_bronze);
                            img_prata.setImageResource(R.drawable.deface_prata);
                            img_ouro.setImageResource(R.drawable.deface_ouro);
                            img_platina.setImageResource(R.drawable.deface_platina);
                            img_black.setImageResource(R.drawable.deface_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.deface_black);
                            img_bronze.setImageResource(R.drawable.deface_bronze);
                            img_prata.setImageResource(R.drawable.deface_prata);
                            img_ouro.setImageResource(R.drawable.deface_ouro);
                            img_platina.setImageResource(R.drawable.deface_platina);
                            img_black.setImageResource(R.drawable.deface_black);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_desafios:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("concluirdesafios").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Desafios");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.desafios_block);
                            img_bronze.setImageResource(R.drawable.desafios_block);
                            img_prata.setImageResource(R.drawable.desafios_block);
                            img_ouro.setImageResource(R.drawable.desafios_block);
                            img_platina.setImageResource(R.drawable.desafios_block);
                            img_black.setImageResource(R.drawable.desafios_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.desafios_bronze);
                            img_bronze.setImageResource(R.drawable.desafios_bronze);
                            img_prata.setImageResource(R.drawable.desafios_block);
                            img_ouro.setImageResource(R.drawable.desafios_block);
                            img_platina.setImageResource(R.drawable.desafios_block);
                            img_black.setImageResource(R.drawable.desafios_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.desafios_prata);
                            img_bronze.setImageResource(R.drawable.desafios_bronze);
                            img_prata.setImageResource(R.drawable.desafios_prata);
                            img_ouro.setImageResource(R.drawable.desafios_block);
                            img_platina.setImageResource(R.drawable.desafios_block);
                            img_black.setImageResource(R.drawable.desafios_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.desafios_ouro);
                            img_bronze.setImageResource(R.drawable.desafios_bronze);
                            img_prata.setImageResource(R.drawable.desafios_prata);
                            img_ouro.setImageResource(R.drawable.desafios_ouro);
                            img_platina.setImageResource(R.drawable.desafios_block);
                            img_black.setImageResource(R.drawable.desafios_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.desafios_platina);
                            img_bronze.setImageResource(R.drawable.desafios_bronze);
                            img_prata.setImageResource(R.drawable.desafios_prata);
                            img_ouro.setImageResource(R.drawable.desafios_ouro);
                            img_platina.setImageResource(R.drawable.desafios_platina);
                            img_black.setImageResource(R.drawable.desafios_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.desafios_black);
                            img_bronze.setImageResource(R.drawable.desafios_bronze);
                            img_prata.setImageResource(R.drawable.desafios_prata);
                            img_ouro.setImageResource(R.drawable.desafios_ouro);
                            img_platina.setImageResource(R.drawable.desafios_platina);
                            img_black.setImageResource(R.drawable.desafios_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_desenvolvedor:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("desenvolvedor").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Desenvolvedor");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.desenvolvedor_block);
                            img_bronze.setImageResource(R.drawable.desenvolvedor_block);
                            img_prata.setImageResource(R.drawable.desenvolvedor_block);
                            img_ouro.setImageResource(R.drawable.desenvolvedor_block);
                            img_platina.setImageResource(R.drawable.desenvolvedor_block);
                            img_black.setImageResource(R.drawable.desenvolvedor_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.desenvolvedor_bronze);
                            img_bronze.setImageResource(R.drawable.desenvolvedor_bronze);
                            img_prata.setImageResource(R.drawable.desenvolvedor_block);
                            img_ouro.setImageResource(R.drawable.desenvolvedor_block);
                            img_platina.setImageResource(R.drawable.desenvolvedor_block);
                            img_black.setImageResource(R.drawable.desenvolvedor_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.desenvolvedor_prata);
                            img_bronze.setImageResource(R.drawable.desenvolvedor_bronze);
                            img_prata.setImageResource(R.drawable.desenvolvedor_prata);
                            img_ouro.setImageResource(R.drawable.desenvolvedor_block);
                            img_platina.setImageResource(R.drawable.desenvolvedor_block);
                            img_black.setImageResource(R.drawable.desenvolvedor_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.desenvolvedor_ouro);
                            img_bronze.setImageResource(R.drawable.desenvolvedor_bronze);
                            img_prata.setImageResource(R.drawable.desenvolvedor_prata);
                            img_ouro.setImageResource(R.drawable.desenvolvedor_ouro);
                            img_platina.setImageResource(R.drawable.desenvolvedor_block);
                            img_black.setImageResource(R.drawable.desenvolvedor_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.desenvolvedor_platina);
                            img_bronze.setImageResource(R.drawable.desenvolvedor_bronze);
                            img_prata.setImageResource(R.drawable.desenvolvedor_prata);
                            img_ouro.setImageResource(R.drawable.desenvolvedor_ouro);
                            img_platina.setImageResource(R.drawable.desenvolvedor_platina);
                            img_black.setImageResource(R.drawable.desenvolvedor_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.desenvolvedor_black);
                            img_bronze.setImageResource(R.drawable.desenvolvedor_bronze);
                            img_prata.setImageResource(R.drawable.desenvolvedor_prata);
                            img_ouro.setImageResource(R.drawable.desenvolvedor_ouro);
                            img_platina.setImageResource(R.drawable.desenvolvedor_platina);
                            img_black.setImageResource(R.drawable.desenvolvedor_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_exploits:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("darexploit").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Exploit");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.exploit_block);
                            img_bronze.setImageResource(R.drawable.exploit_block);
                            img_prata.setImageResource(R.drawable.exploit_block);
                            img_ouro.setImageResource(R.drawable.exploit_block);
                            img_platina.setImageResource(R.drawable.exploit_block);
                            img_black.setImageResource(R.drawable.exploit_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.exploit_bronze);
                            img_bronze.setImageResource(R.drawable.exploit_bronze);
                            img_prata.setImageResource(R.drawable.exploit_block);
                            img_ouro.setImageResource(R.drawable.exploit_block);
                            img_platina.setImageResource(R.drawable.exploit_block);
                            img_black.setImageResource(R.drawable.exploit_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.exploit_prata);
                            img_bronze.setImageResource(R.drawable.exploit_bronze);
                            img_prata.setImageResource(R.drawable.exploit_prata);
                            img_ouro.setImageResource(R.drawable.exploit_block);
                            img_platina.setImageResource(R.drawable.exploit_block);
                            img_black.setImageResource(R.drawable.exploit_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.exploit_ouro);
                            img_bronze.setImageResource(R.drawable.exploit_bronze);
                            img_prata.setImageResource(R.drawable.exploit_prata);
                            img_ouro.setImageResource(R.drawable.exploit_ouro);
                            img_platina.setImageResource(R.drawable.exploit_block);
                            img_black.setImageResource(R.drawable.exploit_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.exploit_platina);
                            img_bronze.setImageResource(R.drawable.exploit_bronze);
                            img_prata.setImageResource(R.drawable.exploit_prata);
                            img_ouro.setImageResource(R.drawable.exploit_ouro);
                            img_platina.setImageResource(R.drawable.exploit_platina);
                            img_black.setImageResource(R.drawable.exploit_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.exploit_black);
                            img_bronze.setImageResource(R.drawable.exploit_bronze);
                            img_prata.setImageResource(R.drawable.exploit_prata);
                            img_ouro.setImageResource(R.drawable.exploit_ouro);
                            img_platina.setImageResource(R.drawable.exploit_platina);
                            img_black.setImageResource(R.drawable.exploit_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_contratos:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("encerrarcontratos").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Contratos");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.contratos_block);
                            img_bronze.setImageResource(R.drawable.contratos_block);
                            img_prata.setImageResource(R.drawable.contratos_block);
                            img_ouro.setImageResource(R.drawable.contratos_block);
                            img_platina.setImageResource(R.drawable.contratos_block);
                            img_black.setImageResource(R.drawable.contratos_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.contratos_bronze);
                            img_bronze.setImageResource(R.drawable.contratos_bronze);
                            img_prata.setImageResource(R.drawable.contratos_block);
                            img_ouro.setImageResource(R.drawable.contratos_block);
                            img_platina.setImageResource(R.drawable.contratos_block);
                            img_black.setImageResource(R.drawable.contratos_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.contratos_prata);
                            img_bronze.setImageResource(R.drawable.contratos_bronze);
                            img_prata.setImageResource(R.drawable.contratos_prata);
                            img_ouro.setImageResource(R.drawable.contratos_block);
                            img_platina.setImageResource(R.drawable.contratos_block);
                            img_black.setImageResource(R.drawable.contratos_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.contratos_ouro);
                            img_bronze.setImageResource(R.drawable.contratos_bronze);
                            img_prata.setImageResource(R.drawable.contratos_prata);
                            img_ouro.setImageResource(R.drawable.contratos_ouro);
                            img_platina.setImageResource(R.drawable.contratos_block);
                            img_black.setImageResource(R.drawable.contratos_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.contratos_platina);
                            img_bronze.setImageResource(R.drawable.contratos_bronze);
                            img_prata.setImageResource(R.drawable.contratos_prata);
                            img_ouro.setImageResource(R.drawable.contratos_ouro);
                            img_platina.setImageResource(R.drawable.contratos_platina);
                            img_black.setImageResource(R.drawable.contratos_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.contratos_black);
                            img_bronze.setImageResource(R.drawable.contratos_bronze);
                            img_prata.setImageResource(R.drawable.contratos_prata);
                            img_ouro.setImageResource(R.drawable.contratos_ouro);
                            img_platina.setImageResource(R.drawable.contratos_platina);
                            img_black.setImageResource(R.drawable.contratos_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_level:
                usuario.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("level").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Level");
                        valortotal.setText(total);
                        black.setText("50");
                        platina.setText("40");
                        ouro.setText("30");
                        prata.setText("20");
                        bronze.setText("10");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 10){
                            img.setImageResource(R.drawable.level_block);
                            img_bronze.setImageResource(R.drawable.level_block);
                            img_prata.setImageResource(R.drawable.level_block);
                            img_ouro.setImageResource(R.drawable.level_block);
                            img_platina.setImageResource(R.drawable.level_block);
                            img_black.setImageResource(R.drawable.level_block);
                        } else if (intTotal >= 10 && intTotal < 20){
                            img.setImageResource(R.drawable.level_bronze);
                            img_bronze.setImageResource(R.drawable.level_bronze);
                            img_prata.setImageResource(R.drawable.level_block);
                            img_ouro.setImageResource(R.drawable.level_block);
                            img_platina.setImageResource(R.drawable.level_block);
                            img_black.setImageResource(R.drawable.level_block);
                        } else if (intTotal >= 20 && intTotal < 30){
                            img.setImageResource(R.drawable.level_prata);
                            img_bronze.setImageResource(R.drawable.level_bronze);
                            img_prata.setImageResource(R.drawable.level_prata);
                            img_ouro.setImageResource(R.drawable.level_block);
                            img_platina.setImageResource(R.drawable.level_block);
                            img_black.setImageResource(R.drawable.level_block);
                        } else if (intTotal >= 30 && intTotal < 40){
                            img.setImageResource(R.drawable.level_ouro);
                            img_bronze.setImageResource(R.drawable.level_bronze);
                            img_prata.setImageResource(R.drawable.level_prata);
                            img_ouro.setImageResource(R.drawable.level_ouro);
                            img_platina.setImageResource(R.drawable.level_block);
                            img_black.setImageResource(R.drawable.level_block);
                        } else if (intTotal >= 40 && intTotal < 50){
                            img.setImageResource(R.drawable.level_platina);
                            img_bronze.setImageResource(R.drawable.level_bronze);
                            img_prata.setImageResource(R.drawable.level_prata);
                            img_ouro.setImageResource(R.drawable.level_ouro);
                            img_platina.setImageResource(R.drawable.level_platina);
                            img_black.setImageResource(R.drawable.level_block);
                        } else if (intTotal >= 50){
                            img.setImageResource(R.drawable.level_black);
                            img_bronze.setImageResource(R.drawable.level_bronze);
                            img_prata.setImageResource(R.drawable.level_prata);
                            img_ouro.setImageResource(R.drawable.level_ouro);
                            img_platina.setImageResource(R.drawable.level_platina);
                            img_black.setImageResource(R.drawable.level_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_mitm:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("fazermitm").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("MITM");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.mitm_block);
                            img_bronze.setImageResource(R.drawable.mitm_block);
                            img_prata.setImageResource(R.drawable.mitm_block);
                            img_ouro.setImageResource(R.drawable.mitm_block);
                            img_platina.setImageResource(R.drawable.mitm_block);
                            img_black.setImageResource(R.drawable.mitm_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.mitm_bronze);
                            img_bronze.setImageResource(R.drawable.mitm_bronze);
                            img_prata.setImageResource(R.drawable.mitm_block);
                            img_ouro.setImageResource(R.drawable.mitm_block);
                            img_platina.setImageResource(R.drawable.mitm_block);
                            img_black.setImageResource(R.drawable.mitm_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.mitm_prata);
                            img_bronze.setImageResource(R.drawable.mitm_bronze);
                            img_prata.setImageResource(R.drawable.mitm_prata);
                            img_ouro.setImageResource(R.drawable.mitm_block);
                            img_platina.setImageResource(R.drawable.mitm_block);
                            img_black.setImageResource(R.drawable.mitm_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.mitm_ouro);
                            img_bronze.setImageResource(R.drawable.mitm_bronze);
                            img_prata.setImageResource(R.drawable.mitm_prata);
                            img_ouro.setImageResource(R.drawable.mitm_ouro);
                            img_platina.setImageResource(R.drawable.mitm_block);
                            img_black.setImageResource(R.drawable.mitm_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.mitm_platina);
                            img_bronze.setImageResource(R.drawable.mitm_bronze);
                            img_prata.setImageResource(R.drawable.mitm_prata);
                            img_ouro.setImageResource(R.drawable.mitm_ouro);
                            img_platina.setImageResource(R.drawable.mitm_platina);
                            img_black.setImageResource(R.drawable.mitm_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.mitm_black);
                            img_bronze.setImageResource(R.drawable.mitm_bronze);
                            img_prata.setImageResource(R.drawable.mitm_prata);
                            img_ouro.setImageResource(R.drawable.mitm_ouro);
                            img_platina.setImageResource(R.drawable.mitm_platina);
                            img_black.setImageResource(R.drawable.mitm_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_phishing:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("fazerphishing").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Phishing");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.phishing_block);
                            img_bronze.setImageResource(R.drawable.phishing_block);
                            img_prata.setImageResource(R.drawable.phishing_block);
                            img_ouro.setImageResource(R.drawable.phishing_block);
                            img_platina.setImageResource(R.drawable.phishing_block);
                            img_black.setImageResource(R.drawable.phishing_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.phishing_bronze);
                            img_bronze.setImageResource(R.drawable.phishing_bronze);
                            img_prata.setImageResource(R.drawable.phishing_block);
                            img_ouro.setImageResource(R.drawable.phishing_block);
                            img_platina.setImageResource(R.drawable.phishing_block);
                            img_black.setImageResource(R.drawable.phishing_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.phishing_prata);
                            img_bronze.setImageResource(R.drawable.phishing_bronze);
                            img_prata.setImageResource(R.drawable.phishing_prata);
                            img_ouro.setImageResource(R.drawable.phishing_block);
                            img_platina.setImageResource(R.drawable.phishing_block);
                            img_black.setImageResource(R.drawable.phishing_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.phishing_ouro);
                            img_bronze.setImageResource(R.drawable.phishing_bronze);
                            img_prata.setImageResource(R.drawable.phishing_prata);
                            img_ouro.setImageResource(R.drawable.phishing_ouro);
                            img_platina.setImageResource(R.drawable.phishing_block);
                            img_black.setImageResource(R.drawable.phishing_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.phishing_platina);
                            img_bronze.setImageResource(R.drawable.phishing_bronze);
                            img_prata.setImageResource(R.drawable.phishing_prata);
                            img_ouro.setImageResource(R.drawable.phishing_ouro);
                            img_platina.setImageResource(R.drawable.phishing_platina);
                            img_black.setImageResource(R.drawable.phishing_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.phishing_black);
                            img_bronze.setImageResource(R.drawable.phishing_bronze);
                            img_prata.setImageResource(R.drawable.phishing_prata);
                            img_ouro.setImageResource(R.drawable.phishing_ouro);
                            img_platina.setImageResource(R.drawable.phishing_platina);
                            img_black.setImageResource(R.drawable.phishing_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_ddos:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("fazerddos").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("DDoS");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.ddos_block);
                            img_bronze.setImageResource(R.drawable.ddos_block);
                            img_prata.setImageResource(R.drawable.ddos_block);
                            img_ouro.setImageResource(R.drawable.ddos_block);
                            img_platina.setImageResource(R.drawable.ddos_block);
                            img_black.setImageResource(R.drawable.ddos_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.ddos_bronze);
                            img_bronze.setImageResource(R.drawable.ddos_bronze);
                            img_prata.setImageResource(R.drawable.ddos_block);
                            img_ouro.setImageResource(R.drawable.ddos_block);
                            img_platina.setImageResource(R.drawable.ddos_block);
                            img_black.setImageResource(R.drawable.ddos_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.ddos_prata);
                            img_bronze.setImageResource(R.drawable.ddos_bronze);
                            img_prata.setImageResource(R.drawable.ddos_prata);
                            img_ouro.setImageResource(R.drawable.ddos_block);
                            img_platina.setImageResource(R.drawable.ddos_block);
                            img_black.setImageResource(R.drawable.ddos_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.ddos_ouro);
                            img_bronze.setImageResource(R.drawable.ddos_bronze);
                            img_prata.setImageResource(R.drawable.ddos_prata);
                            img_ouro.setImageResource(R.drawable.ddos_ouro);
                            img_platina.setImageResource(R.drawable.ddos_block);
                            img_black.setImageResource(R.drawable.ddos_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.ddos_platina);
                            img_bronze.setImageResource(R.drawable.ddos_bronze);
                            img_prata.setImageResource(R.drawable.ddos_prata);
                            img_ouro.setImageResource(R.drawable.ddos_ouro);
                            img_platina.setImageResource(R.drawable.ddos_platina);
                            img_black.setImageResource(R.drawable.ddos_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.ddos_black);
                            img_bronze.setImageResource(R.drawable.ddos_bronze);
                            img_prata.setImageResource(R.drawable.ddos_prata);
                            img_ouro.setImageResource(R.drawable.ddos_ouro);
                            img_platina.setImageResource(R.drawable.ddos_platina);
                            img_black.setImageResource(R.drawable.ddos_black);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_procurados:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("serprocurado").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Procurado");
                        valortotal.setText(total);
                        black.setText("50");
                        platina.setText("40");
                        ouro.setText("30");
                        prata.setText("20");
                        bronze.setText("10");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 10){
                            img.setImageResource(R.drawable.procurado_block);
                            img_bronze.setImageResource(R.drawable.procurado_block);
                            img_prata.setImageResource(R.drawable.procurado_block);
                            img_ouro.setImageResource(R.drawable.procurado_block);
                            img_platina.setImageResource(R.drawable.procurado_block);
                            img_black.setImageResource(R.drawable.procurado_block);
                        } else if (intTotal >= 10 && intTotal < 20){
                            img.setImageResource(R.drawable.procurado_bronze);
                            img_bronze.setImageResource(R.drawable.procurado_bronze);
                            img_prata.setImageResource(R.drawable.procurado_block);
                            img_ouro.setImageResource(R.drawable.procurado_block);
                            img_platina.setImageResource(R.drawable.procurado_block);
                            img_black.setImageResource(R.drawable.procurado_block);
                        } else if (intTotal >= 20 && intTotal < 30){
                            img.setImageResource(R.drawable.procurado_prata);
                            img_bronze.setImageResource(R.drawable.procurado_bronze);
                            img_prata.setImageResource(R.drawable.procurado_prata);
                            img_ouro.setImageResource(R.drawable.procurado_block);
                            img_platina.setImageResource(R.drawable.procurado_block);
                            img_black.setImageResource(R.drawable.procurado_block);
                        } else if (intTotal >= 30 && intTotal < 40){
                            img.setImageResource(R.drawable.procurado_ouro);
                            img_bronze.setImageResource(R.drawable.procurado_bronze);
                            img_prata.setImageResource(R.drawable.procurado_prata);
                            img_ouro.setImageResource(R.drawable.procurado_ouro);
                            img_platina.setImageResource(R.drawable.procurado_block);
                            img_black.setImageResource(R.drawable.procurado_block);
                        } else if (intTotal >= 40 && intTotal < 50){
                            img.setImageResource(R.drawable.procurado_platina);
                            img_bronze.setImageResource(R.drawable.procurado_bronze);
                            img_prata.setImageResource(R.drawable.procurado_prata);
                            img_ouro.setImageResource(R.drawable.procurado_ouro);
                            img_platina.setImageResource(R.drawable.procurado_platina);
                            img_black.setImageResource(R.drawable.procurado_block);
                        } else if (intTotal >= 50){
                            img.setImageResource(R.drawable.procurado_black);
                            img_bronze.setImageResource(R.drawable.procurado_bronze);
                            img_prata.setImageResource(R.drawable.procurado_prata);
                            img_ouro.setImageResource(R.drawable.procurado_ouro);
                            img_platina.setImageResource(R.drawable.procurado_platina);
                            img_black.setImageResource(R.drawable.procurado_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_ransomware:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("fazerransomware").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Ransomware");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.ransomware_block);
                            img_bronze.setImageResource(R.drawable.ransomware_block);
                            img_prata.setImageResource(R.drawable.ransomware_block);
                            img_ouro.setImageResource(R.drawable.ransomware_block);
                            img_platina.setImageResource(R.drawable.ransomware_block);
                            img_black.setImageResource(R.drawable.ransomware_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.ransomware_bronze);
                            img_bronze.setImageResource(R.drawable.ransomware_bronze);
                            img_prata.setImageResource(R.drawable.ransomware_block);
                            img_ouro.setImageResource(R.drawable.ransomware_block);
                            img_platina.setImageResource(R.drawable.ransomware_block);
                            img_black.setImageResource(R.drawable.ransomware_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.ransomware_prata);
                            img_bronze.setImageResource(R.drawable.ransomware_bronze);
                            img_prata.setImageResource(R.drawable.ransomware_prata);
                            img_ouro.setImageResource(R.drawable.ransomware_block);
                            img_platina.setImageResource(R.drawable.ransomware_block);
                            img_black.setImageResource(R.drawable.ransomware_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.ransomware_ouro);
                            img_bronze.setImageResource(R.drawable.ransomware_bronze);
                            img_prata.setImageResource(R.drawable.ransomware_prata);
                            img_ouro.setImageResource(R.drawable.ransomware_ouro);
                            img_platina.setImageResource(R.drawable.ransomware_block);
                            img_black.setImageResource(R.drawable.ransomware_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.ransomware_platina);
                            img_bronze.setImageResource(R.drawable.ransomware_bronze);
                            img_prata.setImageResource(R.drawable.ransomware_prata);
                            img_ouro.setImageResource(R.drawable.ransomware_ouro);
                            img_platina.setImageResource(R.drawable.ransomware_platina);
                            img_black.setImageResource(R.drawable.ransomware_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.ransomware_black);
                            img_bronze.setImageResource(R.drawable.ransomware_bronze);
                            img_prata.setImageResource(R.drawable.ransomware_prata);
                            img_ouro.setImageResource(R.drawable.ransomware_ouro);
                            img_platina.setImageResource(R.drawable.ransomware_platina);
                            img_black.setImageResource(R.drawable.ransomware_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_rastros:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("limparrastros").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Rastros");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.rastros_block);
                            img_bronze.setImageResource(R.drawable.rastros_block);
                            img_prata.setImageResource(R.drawable.rastros_block);
                            img_ouro.setImageResource(R.drawable.rastros_block);
                            img_platina.setImageResource(R.drawable.rastros_block);
                            img_black.setImageResource(R.drawable.rastros_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.rastros_bronze);
                            img_bronze.setImageResource(R.drawable.rastros_bronze);
                            img_prata.setImageResource(R.drawable.rastros_block);
                            img_ouro.setImageResource(R.drawable.rastros_block);
                            img_platina.setImageResource(R.drawable.rastros_block);
                            img_black.setImageResource(R.drawable.rastros_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.rastros_prata);
                            img_bronze.setImageResource(R.drawable.rastros_bronze);
                            img_prata.setImageResource(R.drawable.rastros_prata);
                            img_ouro.setImageResource(R.drawable.rastros_block);
                            img_platina.setImageResource(R.drawable.rastros_block);
                            img_black.setImageResource(R.drawable.rastros_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.rastros_ouro);
                            img_bronze.setImageResource(R.drawable.rastros_bronze);
                            img_prata.setImageResource(R.drawable.rastros_prata);
                            img_ouro.setImageResource(R.drawable.rastros_ouro);
                            img_platina.setImageResource(R.drawable.rastros_block);
                            img_black.setImageResource(R.drawable.rastros_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.rastros_platina);
                            img_bronze.setImageResource(R.drawable.rastros_bronze);
                            img_prata.setImageResource(R.drawable.rastros_prata);
                            img_ouro.setImageResource(R.drawable.rastros_ouro);
                            img_platina.setImageResource(R.drawable.rastros_platina);
                            img_black.setImageResource(R.drawable.rastros_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.rastros_black);
                            img_bronze.setImageResource(R.drawable.rastros_bronze);
                            img_prata.setImageResource(R.drawable.rastros_prata);
                            img_ouro.setImageResource(R.drawable.rastros_ouro);
                            img_platina.setImageResource(R.drawable.rastros_platina);
                            img_black.setImageResource(R.drawable.rastros_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_recrutador:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("recrutar").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Recrutar");
                        valortotal.setText(total);
                        black.setText("50");
                        platina.setText("40");
                        ouro.setText("30");
                        prata.setText("20");
                        bronze.setText("10");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 10){
                            img.setImageResource(R.drawable.recrutador_block);
                            img_bronze.setImageResource(R.drawable.recrutador_block);
                            img_prata.setImageResource(R.drawable.recrutador_block);
                            img_ouro.setImageResource(R.drawable.recrutador_block);
                            img_platina.setImageResource(R.drawable.recrutador_block);
                            img_black.setImageResource(R.drawable.recrutador_block);
                        } else if (intTotal >= 10 && intTotal < 20){
                            img.setImageResource(R.drawable.recrutador_bronze);
                            img_bronze.setImageResource(R.drawable.recrutador_bronze);
                            img_prata.setImageResource(R.drawable.recrutador_block);
                            img_ouro.setImageResource(R.drawable.recrutador_block);
                            img_platina.setImageResource(R.drawable.recrutador_block);
                            img_black.setImageResource(R.drawable.recrutador_block);
                        } else if (intTotal >= 20 && intTotal < 30){
                            img.setImageResource(R.drawable.recrutador_prata);
                            img_bronze.setImageResource(R.drawable.recrutador_bronze);
                            img_prata.setImageResource(R.drawable.recrutador_prata);
                            img_ouro.setImageResource(R.drawable.recrutador_block);
                            img_platina.setImageResource(R.drawable.recrutador_block);
                            img_black.setImageResource(R.drawable.recrutador_block);
                        } else if (intTotal >= 30 && intTotal < 40){
                            img.setImageResource(R.drawable.recrutador_ouro);
                            img_bronze.setImageResource(R.drawable.recrutador_bronze);
                            img_prata.setImageResource(R.drawable.recrutador_prata);
                            img_ouro.setImageResource(R.drawable.recrutador_ouro);
                            img_platina.setImageResource(R.drawable.recrutador_block);
                            img_black.setImageResource(R.drawable.recrutador_block);
                        } else if (intTotal >= 40 && intTotal < 50){
                            img.setImageResource(R.drawable.recrutador_platina);
                            img_bronze.setImageResource(R.drawable.recrutador_bronze);
                            img_prata.setImageResource(R.drawable.recrutador_prata);
                            img_ouro.setImageResource(R.drawable.recrutador_ouro);
                            img_platina.setImageResource(R.drawable.recrutador_platina);
                            img_black.setImageResource(R.drawable.recrutador_block);
                        } else if (intTotal >= 50){
                            img.setImageResource(R.drawable.recrutador_black);
                            img_bronze.setImageResource(R.drawable.recrutador_bronze);
                            img_prata.setImageResource(R.drawable.recrutador_prata);
                            img_ouro.setImageResource(R.drawable.recrutador_ouro);
                            img_platina.setImageResource(R.drawable.recrutador_platina);
                            img_black.setImageResource(R.drawable.recrutador_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_decrypt:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("fazerdecrypt").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Decrypt");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.decrypt_block);
                            img_bronze.setImageResource(R.drawable.decrypt_block);
                            img_prata.setImageResource(R.drawable.decrypt_block);
                            img_ouro.setImageResource(R.drawable.decrypt_block);
                            img_platina.setImageResource(R.drawable.decrypt_block);
                            img_black.setImageResource(R.drawable.decrypt_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.decrypt_bronze);
                            img_bronze.setImageResource(R.drawable.decrypt_bronze);
                            img_prata.setImageResource(R.drawable.decrypt_block);
                            img_ouro.setImageResource(R.drawable.decrypt_block);
                            img_platina.setImageResource(R.drawable.decrypt_block);
                            img_black.setImageResource(R.drawable.decrypt_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.decrypt_prata);
                            img_bronze.setImageResource(R.drawable.decrypt_bronze);
                            img_prata.setImageResource(R.drawable.decrypt_prata);
                            img_ouro.setImageResource(R.drawable.decrypt_block);
                            img_platina.setImageResource(R.drawable.decrypt_block);
                            img_black.setImageResource(R.drawable.decrypt_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.decrypt_ouro);
                            img_bronze.setImageResource(R.drawable.decrypt_bronze);
                            img_prata.setImageResource(R.drawable.decrypt_prata);
                            img_ouro.setImageResource(R.drawable.decrypt_ouro);
                            img_platina.setImageResource(R.drawable.decrypt_block);
                            img_black.setImageResource(R.drawable.decrypt_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.decrypt_platina);
                            img_bronze.setImageResource(R.drawable.decrypt_bronze);
                            img_prata.setImageResource(R.drawable.decrypt_prata);
                            img_ouro.setImageResource(R.drawable.decrypt_ouro);
                            img_platina.setImageResource(R.drawable.decrypt_platina);
                            img_black.setImageResource(R.drawable.decrypt_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.decrypt_black);
                            img_bronze.setImageResource(R.drawable.decrypt_bronze);
                            img_prata.setImageResource(R.drawable.decrypt_prata);
                            img_ouro.setImageResource(R.drawable.decrypt_ouro);
                            img_platina.setImageResource(R.drawable.decrypt_platina);
                            img_black.setImageResource(R.drawable.decrypt_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_roubos:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("roubar").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Roubos");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.roubos_block);
                            img_bronze.setImageResource(R.drawable.roubos_block);
                            img_prata.setImageResource(R.drawable.roubos_block);
                            img_ouro.setImageResource(R.drawable.roubos_block);
                            img_platina.setImageResource(R.drawable.roubos_block);
                            img_black.setImageResource(R.drawable.roubos_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.roubos_bronze);
                            img_bronze.setImageResource(R.drawable.roubos_bronze);
                            img_prata.setImageResource(R.drawable.roubos_block);
                            img_ouro.setImageResource(R.drawable.roubos_block);
                            img_platina.setImageResource(R.drawable.roubos_block);
                            img_black.setImageResource(R.drawable.roubos_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.roubos_prata);
                            img_bronze.setImageResource(R.drawable.roubos_bronze);
                            img_prata.setImageResource(R.drawable.roubos_prata);
                            img_ouro.setImageResource(R.drawable.roubos_block);
                            img_platina.setImageResource(R.drawable.roubos_block);
                            img_black.setImageResource(R.drawable.roubos_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.roubos_ouro);
                            img_bronze.setImageResource(R.drawable.roubos_bronze);
                            img_prata.setImageResource(R.drawable.roubos_prata);
                            img_ouro.setImageResource(R.drawable.roubos_ouro);
                            img_platina.setImageResource(R.drawable.roubos_block);
                            img_black.setImageResource(R.drawable.roubos_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.roubos_platina);
                            img_bronze.setImageResource(R.drawable.roubos_bronze);
                            img_prata.setImageResource(R.drawable.roubos_prata);
                            img_ouro.setImageResource(R.drawable.roubos_ouro);
                            img_platina.setImageResource(R.drawable.roubos_platina);
                            img_black.setImageResource(R.drawable.roubos_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.roubos_black);
                            img_bronze.setImageResource(R.drawable.roubos_bronze);
                            img_prata.setImageResource(R.drawable.roubos_prata);
                            img_ouro.setImageResource(R.drawable.roubos_ouro);
                            img_platina.setImageResource(R.drawable.roubos_platina);
                            img_black.setImageResource(R.drawable.roubos_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_trabalho:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("terminartrabalho").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Trabalho");
                        valortotal.setText(total);
                        black.setText("5000");
                        platina.setText("4000");
                        ouro.setText("3000");
                        prata.setText("2000");
                        bronze.setText("1000");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 1000){
                            img.setImageResource(R.drawable.trabalho_block);
                            img_bronze.setImageResource(R.drawable.trabalho_block);
                            img_prata.setImageResource(R.drawable.trabalho_block);
                            img_ouro.setImageResource(R.drawable.trabalho_block);
                            img_platina.setImageResource(R.drawable.trabalho_block);
                            img_black.setImageResource(R.drawable.trabalho_block);
                        } else if (intTotal >= 1000 && intTotal < 2000){
                            img.setImageResource(R.drawable.trabalho_bronze);
                            img_bronze.setImageResource(R.drawable.trabalho_bronze);
                            img_prata.setImageResource(R.drawable.trabalho_block);
                            img_ouro.setImageResource(R.drawable.trabalho_block);
                            img_platina.setImageResource(R.drawable.trabalho_block);
                            img_black.setImageResource(R.drawable.trabalho_block);
                        } else if (intTotal >= 2000 && intTotal < 3000){
                            img.setImageResource(R.drawable.trabalho_prata);
                            img_bronze.setImageResource(R.drawable.trabalho_bronze);
                            img_prata.setImageResource(R.drawable.trabalho_prata);
                            img_ouro.setImageResource(R.drawable.trabalho_block);
                            img_platina.setImageResource(R.drawable.trabalho_block);
                            img_black.setImageResource(R.drawable.trabalho_block);
                        } else if (intTotal >= 3000 && intTotal < 4000){
                            img.setImageResource(R.drawable.trabalho_ouro);
                            img_bronze.setImageResource(R.drawable.trabalho_bronze);
                            img_prata.setImageResource(R.drawable.trabalho_prata);
                            img_ouro.setImageResource(R.drawable.trabalho_ouro);
                            img_platina.setImageResource(R.drawable.trabalho_block);
                            img_black.setImageResource(R.drawable.trabalho_block);
                        } else if (intTotal >= 4000 && intTotal < 5000){
                            img.setImageResource(R.drawable.trabalho_platina);
                            img_bronze.setImageResource(R.drawable.trabalho_bronze);
                            img_prata.setImageResource(R.drawable.trabalho_prata);
                            img_ouro.setImageResource(R.drawable.trabalho_ouro);
                            img_platina.setImageResource(R.drawable.trabalho_platina);
                            img_black.setImageResource(R.drawable.trabalho_block);
                        } else if (intTotal >= 5000){
                            img.setImageResource(R.drawable.trabalho_black);
                            img_bronze.setImageResource(R.drawable.trabalho_bronze);
                            img_prata.setImageResource(R.drawable.trabalho_prata);
                            img_ouro.setImageResource(R.drawable.trabalho_ouro);
                            img_platina.setImageResource(R.drawable.trabalho_platina);
                            img_black.setImageResource(R.drawable.trabalho_black);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
            case R.id.btn_eventos:
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String total = dataSnapshot.child("participareventos").getValue().toString();

                        ImageView img = myDialog.findViewById(R.id.img_principal);
                        TextView titulo = myDialog.findViewById(R.id.text_usuario);
                        TextView valortotal = myDialog.findViewById(R.id.text_total);
                        ImageView img_black = myDialog.findViewById(R.id.img_black);
                        TextView black = myDialog.findViewById(R.id.label_btc);
                        ImageView img_platina = myDialog.findViewById(R.id.img_platina);
                        TextView platina = myDialog.findViewById(R.id.text_platina);
                        ImageView img_ouro = myDialog.findViewById(R.id.img_ouro);
                        TextView ouro = myDialog.findViewById(R.id.text_ouro);
                        ImageView img_prata = myDialog.findViewById(R.id.img_prata);
                        TextView prata = myDialog.findViewById(R.id.label_money);
                        ImageView img_bronze = myDialog.findViewById(R.id.img_bronze);
                        TextView bronze = myDialog.findViewById(R.id.text_bronze);

                        titulo.setText("Eventos");
                        valortotal.setText(total);
                        black.setText("50");
                        platina.setText("40");
                        ouro.setText("30");
                        prata.setText("20");
                        bronze.setText("10");

                        int intTotal = Integer.parseInt(total);

                        if (intTotal < 10){
                            img.setImageResource(R.drawable.eventos_block);
                            img_bronze.setImageResource(R.drawable.eventos_block);
                            img_prata.setImageResource(R.drawable.eventos_block);
                            img_ouro.setImageResource(R.drawable.eventos_block);
                            img_platina.setImageResource(R.drawable.eventos_block);
                            img_black.setImageResource(R.drawable.eventos_block);
                        } else if (intTotal >= 10 && intTotal < 20){
                            img.setImageResource(R.drawable.eventos_bronze);
                            img_bronze.setImageResource(R.drawable.eventos_bronze);
                            img_prata.setImageResource(R.drawable.eventos_block);
                            img_ouro.setImageResource(R.drawable.eventos_block);
                            img_platina.setImageResource(R.drawable.eventos_block);
                            img_black.setImageResource(R.drawable.eventos_block);
                        } else if (intTotal >= 20 && intTotal < 30){
                            img.setImageResource(R.drawable.eventos_prata);
                            img_bronze.setImageResource(R.drawable.eventos_bronze);
                            img_prata.setImageResource(R.drawable.eventos_prata);
                            img_ouro.setImageResource(R.drawable.eventos_block);
                            img_platina.setImageResource(R.drawable.eventos_block);
                            img_black.setImageResource(R.drawable.eventos_block);
                        } else if (intTotal >= 30 && intTotal < 40){
                            img.setImageResource(R.drawable.eventos_ouro);
                            img_bronze.setImageResource(R.drawable.eventos_bronze);
                            img_prata.setImageResource(R.drawable.eventos_prata);
                            img_ouro.setImageResource(R.drawable.eventos_ouro);
                            img_platina.setImageResource(R.drawable.eventos_block);
                            img_black.setImageResource(R.drawable.eventos_block);
                        } else if (intTotal >= 40 && intTotal < 50){
                            img.setImageResource(R.drawable.eventos_platina);
                            img_bronze.setImageResource(R.drawable.eventos_bronze);
                            img_prata.setImageResource(R.drawable.eventos_prata);
                            img_ouro.setImageResource(R.drawable.eventos_ouro);
                            img_platina.setImageResource(R.drawable.eventos_platina);
                            img_black.setImageResource(R.drawable.eventos_block);
                        } else if (intTotal >= 50){
                            img.setImageResource(R.drawable.eventos_black);
                            img_bronze.setImageResource(R.drawable.eventos_bronze);
                            img_prata.setImageResource(R.drawable.eventos_prata);
                            img_ouro.setImageResource(R.drawable.eventos_ouro);
                            img_platina.setImageResource(R.drawable.eventos_platina);
                            img_black.setImageResource(R.drawable.eventos_black);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                myDialog.show();
            break;
        }
    }

    class MyAdapter extends ArrayAdapter<String>{
        Context context;
        String[] list_acoes;
        String[] list_realizados;

        MyAdapter(Context c, String[] acoes, String[] realizados){
            super(c, R.layout.listview_perfil, R.id.label_acoes, acoes);
            this.context = c;
            this.list_acoes = acoes;
            this.list_realizados = realizados;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_perfil, parent, false);

            TextView acoes = row.findViewById(R.id.label_acoes);
            TextView realizados = row.findViewById(R.id.label_realizados);

            acoes.setText(list_acoes[position]);
            realizados.setText(list_realizados[position]);

            return row;
        }
    }
}
