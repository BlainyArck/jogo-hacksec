package com.blainyarckprojects.hacksec;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText input_usuario, input_senha;
    private Button btn_login;
    private ProgressBar login_progressbar;
    private FirebaseAuth mAuth;
    private Intent InicioActivity;
    private TextView label_criarconta, label_esqueceu_senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        input_usuario = findViewById(R.id.input_usuario);
        input_senha = findViewById(R.id.input_senha);
        btn_login = findViewById(R.id.btn_login);
        login_progressbar = findViewById(R.id.login_progressbar);
        mAuth = FirebaseAuth.getInstance();
        InicioActivity = new Intent(this,com.blainyarckprojects.hacksec.InicioActivity.class);
        label_criarconta = findViewById(R.id.label_criar_conta);
        label_esqueceu_senha = findViewById(R.id.label_esqueceu_senha);

        login_progressbar.setVisibility(View.INVISIBLE);

        btn_login.setOnClickListener(this);
        label_criarconta.setOnClickListener(this);
        label_esqueceu_senha.setOnClickListener(this);

    }

    private void logar(String usuario, String senha) {
        mAuth.signInWithEmailAndPassword(usuario,senha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    login_progressbar.setVisibility(View.INVISIBLE);
                    btn_login.setVisibility(View.VISIBLE);

                    Acesso();
                }else{
                    String resp = task.getException().toString();
                    Functions.opcoesErro(getBaseContext(), resp);
                    btn_login.setVisibility(View.VISIBLE);
                    login_progressbar.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void Acesso() {
        startActivity(InicioActivity);
        finish();
    }

    private void showMessage(String text) {
        Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null){
            Acesso();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                login_progressbar.setVisibility(View.VISIBLE);
                btn_login.setVisibility(View.INVISIBLE);

                final String usuario = input_usuario.getText().toString();
                final String senha = input_senha.getText().toString();

                if (usuario.isEmpty() || senha.isEmpty()){
                    showMessage("Verifique os campos digitados");
                    btn_login.setVisibility(View.VISIBLE);
                    login_progressbar.setVisibility(View.INVISIBLE);
                }else{
                    logar(usuario,senha);
                }
            break;

            case R.id.label_criar_conta:
                startActivity(new Intent(this, RegistrarActivity.class));
            break;

            case R.id.label_esqueceu_senha:
                startActivity(new Intent(this, RecuperarSenhaActivity.class));
            break;
        }
    }
}