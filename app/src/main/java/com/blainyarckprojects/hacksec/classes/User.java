package com.blainyarckprojects.hacksec.classes;

public class User {
    public String email;
    public String usuario;
    public Integer level;
    public Integer reputacao;
    public String enderecoip;
    public String conexao;
    public Integer money;
    public Integer btc;
    public Integer boosters;
    public String grupohacker;
    public String cargo;
    public String classe;
    public String carteira;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String email, String usuario, Integer level, Integer reputacao,
                String enderecoip, String conexao, Integer money, Integer btc,
                Integer boosters, String grupohacker, String cargo, String classe, String carteira) {
        this.email = email;
        this.usuario = usuario;
        this.level = level;
        this.reputacao = reputacao;
        this.enderecoip = enderecoip;
        this.conexao = conexao;
        this.money = money;
        this.btc = btc;
        this.boosters = boosters;
        this.grupohacker = grupohacker;
        this.cargo = cargo;
        this.classe = classe;
        this.carteira = carteira;
    }
}
