package com.blainyarckprojects.hacksec.classes;

public class ClassePerfil {
    public Integer ativarbot;
    public Integer fazerdeface;
    public Integer concluirdesafios;
    public Integer desenvolvedor;
    public Integer darexploit;
    public Integer encerrarcontratos;
    public Integer fazermitm;
    public Integer fazerphishing;
    public Integer fazerddos;
    public Integer serprocurado;
    public Integer fazerransomware;
    public Integer limparrastros;
    public Integer recrutar;
    public Integer fazerdecrypt;
    public Integer roubar;
    public Integer terminartrabalho;
    public Integer participareventos;

    public ClassePerfil(){}

    public ClassePerfil(Integer ativarbot, Integer fazerdeface, Integer concluirdesafios, Integer desenvolvedor,
                        Integer darexploit, Integer encerrarcontratos, Integer fazermitm, Integer fazerphishing,
                        Integer fazerddos, Integer serprocurado, Integer fazerransomware, Integer limparrastros,
                        Integer recrutar, Integer fazerdecrypt, Integer roubar, Integer terminartrabalho,
                        Integer participareventos) {
        this.ativarbot = ativarbot;
        this.fazerdeface = fazerdeface;
        this.concluirdesafios = concluirdesafios;
        this.desenvolvedor = desenvolvedor;
        this.darexploit = darexploit;
        this.encerrarcontratos = encerrarcontratos;
        this.fazermitm = fazermitm;
        this.fazerphishing = fazerphishing;
        this.fazerddos = fazerddos;
        this.serprocurado = serprocurado;
        this.fazerransomware = fazerransomware;
        this.limparrastros = limparrastros;
        this.recrutar = recrutar;
        this.fazerdecrypt = fazerdecrypt;
        this.roubar = roubar;
        this.terminartrabalho = terminartrabalho;
        this.participareventos = participareventos;
    }
}
