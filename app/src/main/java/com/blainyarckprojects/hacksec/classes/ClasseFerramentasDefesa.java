package com.blainyarckprojects.hacksec.classes;

public class ClasseFerramentasDefesa {
    public Integer firewall;
    public Integer antivpn;
    public Integer criptografia;
    public Integer antiransomware;
    public Integer mitmprotection;
    public Integer ddosprotection;
    public Integer antivirus;
    public Integer antiphishing;
    public Integer defaceprotection;
    public Integer custo;

    public ClasseFerramentasDefesa(){}

    public ClasseFerramentasDefesa(Integer firewall, Integer antivpn, Integer criptografia,
                                   Integer antiransomware, Integer mitmprotection, Integer ddosprotection,
                                   Integer antivirus, Integer antiphishing, Integer defaceprotection, Integer custo) {
        this.firewall = firewall;
        this.antivpn = antivpn;
        this.criptografia = criptografia;
        this.antiransomware = antiransomware;
        this.mitmprotection = mitmprotection;
        this.ddosprotection = ddosprotection;
        this.antivirus = antivirus;
        this.antiphishing = antiphishing;
        this.defaceprotection = defaceprotection;
        this.custo = custo;
    }
}
