package com.blainyarckprojects.hacksec.classes;

public class ClasseFerramentasAtaque {
    public Integer exploit;
    public Integer vpn;
    public Integer decrypt;
    public Integer ransomware;
    public Integer mitm;
    public Integer ddos;
    public Integer botnet;
    public Integer phishing;
    public Integer deface;
    public Integer custo;

    public ClasseFerramentasAtaque(){}

    public ClasseFerramentasAtaque(Integer exploit, Integer vpn, Integer decrypt,
                                   Integer ransomware, Integer mitm, Integer ddos, Integer botnet,
                                   Integer phishing, Integer deface, Integer custo) {
        this.exploit = exploit;
        this.vpn = vpn;
        this.decrypt = decrypt;
        this.ransomware = ransomware;
        this.mitm = mitm;
        this.ddos = ddos;
        this.botnet = botnet;
        this.phishing = phishing;
        this.deface = deface;
        this.custo = custo;
    }
}