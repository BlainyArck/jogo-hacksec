package com.blainyarckprojects.hacksec.classes;

public class ClasseCriarGrupoHacker {
    public String nome;
    public String tag;
    public String fundador;
    public Integer membros;
    public Integer reputacao;
    public Integer ranking;

    public ClasseCriarGrupoHacker(){}

    public ClasseCriarGrupoHacker(String nome, String tag, String fundador,
                                  Integer membros, Integer reputacao, Integer ranking) {
        this.nome = nome;
        this.tag = tag;
        this.fundador = fundador;
        this.membros = membros;
        this.reputacao = reputacao;
        this.ranking = ranking;
    }
}
