package com.blainyarckprojects.hacksec;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class SnifferActivity extends AppCompatActivity {

    private ImageView btn_server_botnet, btn_heist, btn_bankattack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sniffer);

        btn_server_botnet = findViewById(R.id.btn_server_botnet);
        btn_heist = findViewById(R.id.btn_heist);
        btn_bankattack = findViewById(R.id.btn_bankattack);

        btn_server_botnet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SnifferActivity.this, ServerActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_heist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SnifferActivity.this, HeistActivity.class);
                startActivity(intent);
            }
        });

        btn_bankattack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SnifferActivity.this, BankAttackActivity.class);
                startActivity(intent);
            }
        });
    }
}
