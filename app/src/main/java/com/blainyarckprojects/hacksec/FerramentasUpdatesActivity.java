package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FerramentasUpdatesActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_ferramentas_ataque, btn_ferramentas_defesa;
    private ListView listview_updates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ferramentas_updates);

        btn_ferramentas_ataque = findViewById(R.id.btn_ferramentas_ataque);
        btn_ferramentas_defesa = findViewById(R.id.btn_ferramentas_defesa);
        listview_updates = findViewById(R.id.listview_updates);

        int[] list_img = {R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos,
                R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos};
        String[] list_nome = {"DDoS", "DDoS", "DDoS", "DDoS", "DDoS", "DDoS", "DDoS", "DDoS", "DDoS", "DDoS"};
        String[] list_level = {"10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)"};
        String[] list_porcentagem = {"60%", "60%", "60%", "60%", "60%", "60%", "60%", "60%", "60%", "60%"};

        btn_ferramentas_ataque.setOnClickListener(this);
        btn_ferramentas_defesa.setOnClickListener(this);

        MyAdapter adapter = new MyAdapter(this, list_img, list_nome, list_level, list_porcentagem);

        listview_updates.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_ferramentas_ataque:
                startActivity(new Intent(this, FerramentasAtaqueActivity.class));
                finish();
            break;
            case R.id.btn_ferramentas_defesa:
                startActivity(new Intent(this, FerramentasDefesaActivity.class));
                finish();
            break;
        }
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        int list_img[];
        String list_nome[];
        String list_level[];
        String list_porcentagem[];

        MyAdapter(Context c, int[] imgs, String[] nome, String[] level, String[] porcentagem){
            super(c, R.layout.listview_privado, R.id.label_de, nome);
            this.context = c;
            this.list_img = imgs;
            this.list_nome = nome;
            this.list_level = level;
            this.list_porcentagem = porcentagem;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_updates, parent, false);

            ImageView imgs = row.findViewById(R.id.img_ferramenta);
            TextView nome = row.findViewById(R.id.nome_ferramenta);
            TextView level = row.findViewById(R.id.level_ferramenta);
            TextView porcentagem = row.findViewById(R.id.porcentagem_ferramenta);


            imgs.setImageResource(list_img[position]);
            nome.setText(list_nome[position]);
            level.setText(list_level[position]);
            porcentagem.setText(list_porcentagem[position]);
            return row;
        }
    }
}
