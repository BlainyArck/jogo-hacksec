package com.blainyarckprojects.hacksec;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;

public class RecuperarSenhaActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_recuperar;
    private EditText email_login;
    private FirebaseAuth mAuth;
    private TextView label_criar_conta, label_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_senha);

        btn_recuperar = findViewById(R.id.btn_recuperar);
        email_login = findViewById(R.id.input_usuario);

        label_criar_conta = findViewById(R.id.label_criar_conta);
        label_login = findViewById(R.id.label_login);

        mAuth = FirebaseAuth.getInstance();

        btn_recuperar.setOnClickListener(this);
        label_criar_conta.setOnClickListener(this);
        label_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_recuperar:
                recuperarSenha();
            break;

            case R.id.label_criar_conta:
                startActivity(new Intent(this, RegistrarActivity.class));
            break;

            case R.id.label_login:
                startActivity(new Intent(this, LoginActivity.class));
            break;
        }
    }

    private void recuperarSenha() {

        String email = email_login.getText().toString().trim();

        if (email.isEmpty()){
            Toast.makeText(getBaseContext(), "Digite seu e-mail.", Toast.LENGTH_LONG).show();
        } else {
            enviarEmail(email);
        }
    }

    private void enviarEmail(String email) {
        mAuth.sendPasswordResetEmail(email).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getBaseContext(), "E-mail de recuperação enviado", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                String erro = e.toString();
                Functions.opcoesErro(getBaseContext(), erro);
            }
        });
    }
}
