package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EscolherClasseActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView btn_whitehat, btn_greyhat, btn_blackhat;
    private DatabaseReference mDatabase;

    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escolher_classe);

        btn_whitehat = findViewById(R.id.btn_whitehat);
        btn_blackhat = findViewById(R.id.btn_blackhat);
        btn_greyhat = findViewById(R.id.btn_greyhat);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layoutclasse);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mDatabase = FirebaseDatabase.getInstance().getReference("Users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        btn_whitehat.setOnClickListener(this);
        btn_greyhat.setOnClickListener(this);
        btn_blackhat.setOnClickListener(this);
    }

    private void showMessage(String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }

    private void Registrado() {
        Intent intent = new Intent(EscolherClasseActivity.this,InicioActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_whitehat:
                escolherWhitehat();
            break;
            case R.id.btn_greyhat:
                escolherGreyhat();
            break;
            case R.id.btn_blackhat:
                escolherBlackhat();
            break;
        }
    }

    private void escolherBlackhat() {
        Button btn_fechar;
        Button btn_escolher;

        btn_fechar = myDialog.findViewById(R.id.btn_fechar);
        btn_escolher = myDialog.findViewById(R.id.btn_escolher);

        ImageView imgblack = myDialog.findViewById(R.id.classe_img);
        TextView titulo = myDialog.findViewById(R.id.text_usuario);
        TextView descricao = myDialog.findViewById(R.id.classe_descricao);

        imgblack.setImageResource(R.drawable.icone_blackhat);
        titulo.setText("BlackHat");
        descricao.setText("Escolher opção BlackHat");

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        btn_escolher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String classe = "BlackHat";
                        dataSnapshot.getRef().child("classe").setValue(classe).toString();
                        Registrado();
                        showMessage("Registrado com sucesso!");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        myDialog.show();
    }

    private void escolherGreyhat() {
        Button btn_fechar;
        Button btn_escolher;

        btn_fechar = myDialog.findViewById(R.id.btn_fechar);
        btn_escolher = myDialog.findViewById(R.id.btn_escolher);

        ImageView imgblack = myDialog.findViewById(R.id.classe_img);
        TextView titulo = myDialog.findViewById(R.id.text_usuario);
        TextView descricao = myDialog.findViewById(R.id.classe_descricao);

        imgblack.setImageResource(R.drawable.icone_greyhat);
        titulo.setText("GreyHat");
        descricao.setText("Escolher opção GreyHat");

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        btn_escolher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String classe = "GreyHat";
                        dataSnapshot.getRef().child("classe").setValue(classe).toString();
                        Registrado();
                        showMessage("Registrado com sucesso!");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        myDialog.show();
    }

    private void escolherWhitehat() {
        Button btn_fechar;
        Button btn_escolher;

        btn_fechar = myDialog.findViewById(R.id.btn_fechar);
        btn_escolher = myDialog.findViewById(R.id.btn_escolher);

        ImageView imgwhite = myDialog.findViewById(R.id.classe_img);
        TextView titulo = myDialog.findViewById(R.id.text_usuario);
        TextView descricao = myDialog.findViewById(R.id.classe_descricao);

        imgwhite.setImageResource(R.drawable.icone_whitehat);
        titulo.setText("WhiteHat");
        descricao.setText("Escolher opção WhiteHat");

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        btn_escolher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String classe = "WhiteHat";
                        dataSnapshot.getRef().child("classe").setValue(classe).toString();
                        Registrado();
                        showMessage("Registrado com sucesso!");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        myDialog.show();
    }
}
