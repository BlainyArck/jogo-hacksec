package com.blainyarckprojects.hacksec;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DispositivoActivity extends AppCompatActivity {

    private ImageView btn_sair;
    private TextView label_usuario, label_email;
    private FirebaseAuth sair;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispositivo);

        btn_sair = findViewById(R.id.btn_sair);
        label_usuario = findViewById(R.id.label_usuario);
        label_email = findViewById(R.id.label_email);

        sair = FirebaseAuth.getInstance();

        btn_sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DispositivoActivity.this, LoginActivity.class);
                startActivity(intent);
                sair.signOut();
                finish();
            }
        });

        buscarDados();
    }

    private void buscarDados() {
        mDatabase = FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String usuario = dataSnapshot.child("usuario").getValue().toString();
                String email = dataSnapshot.child("email").getValue().toString();

                label_usuario.setText(usuario);
                label_email.setText(email);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}