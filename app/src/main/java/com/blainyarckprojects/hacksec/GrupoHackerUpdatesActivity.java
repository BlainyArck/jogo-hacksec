package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GrupoHackerUpdatesActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_grupo_hacker, btn_comunicacao, btn_gh_ferramentas, btn_atacar_alvo, btn_ferramentas;
    private TextView label_nome, label_tag, label_fundador, label_reputacao,
            label_ranking, label_membros;
    private ListView listview_updates;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupo_hacker_updates);
        btn_grupo_hacker = findViewById(R.id.btn_grupo_hacker);
        btn_comunicacao = findViewById(R.id.btn_comunicacao);
        btn_gh_ferramentas = findViewById(R.id.btn_gh_ferramentas);

        btn_atacar_alvo = findViewById(R.id.btn_atacar_alvo);
        btn_ferramentas = findViewById(R.id.btn_ferramentas);

        label_nome = findViewById(R.id.label_titulo);
        label_tag = findViewById(R.id.label_tag);
        label_fundador = findViewById(R.id.label_fundador);
        label_reputacao = findViewById(R.id.label_reputacao);
        label_ranking = findViewById(R.id.label_ranking);
        label_membros = findViewById(R.id.label_membros);

        listview_updates = findViewById(R.id.listview_updates);

        int[] list_img = {R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos,
                R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos, R.drawable.icone_ddos};
        String[] list_nome = {"DDoS", "DDoS", "DDoS", "DDoS", "DDoS", "DDoS", "DDoS", "DDoS", "DDoS", "DDoS"};
        String[] list_level = {"10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)", "10 (+1)"};
        String[] list_porcentagem = {"60%", "60%", "60%", "60%", "60%", "60%", "60%", "60%", "60%", "60%"};

        btn_grupo_hacker.setOnClickListener(this);
        btn_comunicacao.setOnClickListener(this);
        btn_gh_ferramentas.setOnClickListener(this);

        btn_atacar_alvo.setOnClickListener(this);
        btn_ferramentas.setOnClickListener(this);

        buscarGrupoHacker();

        MyAdapter adapter = new MyAdapter(this, list_img, list_nome, list_level, list_porcentagem);

        listview_updates.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_grupo_hacker:
                startActivity(new Intent(this, GrupoHackerActivity.class));
                finish();
                break;
            case R.id.btn_comunicacao:
                startActivity(new Intent(this, GrupoHackerComunicacaoActivity.class));
                finish();
                break;
            case R.id.btn_gh_ferramentas:
                startActivity(new Intent(this, GrupoHackerActivity.class));
                finish();
                break;
            case R.id.btn_atacar_alvo:
                startActivity(new Intent(this, GrupoHackerAlvoActivity.class));
                finish();
            break;
            case R.id.btn_ferramentas:
                startActivity(new Intent(this, GrupoHackerFerramentasActivity.class));
                finish();
            break;
        }
    }

    public void buscarGrupoHacker(){
        mDatabase = FirebaseDatabase.getInstance().getReference("GrupoHacker")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String nome = dataSnapshot.child("nome").getValue().toString();
                String tag = dataSnapshot.child("tag").getValue().toString();
                String fundador = dataSnapshot.child("fundador").getValue().toString();
                String ranking = dataSnapshot.child("ranking").getValue().toString();
                String membros = dataSnapshot.child("membros").getValue().toString();
                String reputacao = dataSnapshot.child("reputacao").getValue().toString();

                label_nome.setText(nome);
                label_tag.setText(tag);
                label_fundador.setText(fundador);
                label_ranking.setText(ranking);
                label_membros.setText(membros);
                label_reputacao.setText(reputacao);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        int list_img[];
        String list_nome[];
        String list_level[];
        String list_porcentagem[];

        MyAdapter(Context c, int[] imgs, String[] nome, String[] level, String[] porcentagem){
            super(c, R.layout.listview_privado, R.id.label_de, nome);
            this.context = c;
            this.list_img = imgs;
            this.list_nome = nome;
            this.list_level = level;
            this.list_porcentagem = porcentagem;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_updates, parent, false);

            ImageView imgs = row.findViewById(R.id.img_ferramenta);
            TextView nome = row.findViewById(R.id.nome_ferramenta);
            TextView level = row.findViewById(R.id.level_ferramenta);
            TextView porcentagem = row.findViewById(R.id.porcentagem_ferramenta);


            imgs.setImageResource(list_img[position]);
            nome.setText(list_nome[position]);
            level.setText(list_level[position]);
            porcentagem.setText(list_porcentagem[position]);
            return row;
        }
    }
}
