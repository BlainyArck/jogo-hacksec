package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MercadoNegroActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_mnegro_trabalho, btn_mnegro_contratos;
    ImageView btn_codigo, btn_alterarip;
    Dialog codigo, alterar_ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mercado_negro);

        btn_mnegro_trabalho = findViewById(R.id.btn_mnegro_trabalho);
        btn_mnegro_contratos = findViewById(R.id.btn_mnegro_contratos);
        btn_codigo = findViewById(R.id.btn_codigo);
        btn_alterarip = findViewById(R.id.btn_alterarip);

        codigo = new Dialog(this);
        codigo.setContentView(R.layout.layoutcodigo);
        codigo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alterar_ip = new Dialog(this);
        alterar_ip.setContentView(R.layout.layoutalterarip);
        alterar_ip.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        btn_codigo.setOnClickListener(this);
        btn_alterarip.setOnClickListener(this);

        btn_mnegro_trabalho.setOnClickListener(this);
        btn_mnegro_contratos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_codigo:
                buttonCodigo();
            break;
            case R.id.btn_alterarip:
                buttonAlterarIP();
            break;
            case R.id.btn_mnegro_trabalho:
                startActivity(new Intent(this, TrabalhoActivity.class));
                finish();
            break;
            case R.id.btn_mnegro_contratos:
                startActivity(new Intent(this, ContratosActivity.class));
                finish();
            break;
        }
    }

    private void buttonAlterarIP() {
        Button btn_fechar;
        btn_fechar = alterar_ip.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alterar_ip.dismiss();
            }
        });
        alterar_ip.show();
    }

    private void buttonCodigo() {
        Button btn_fechar;
        btn_fechar = codigo.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codigo.dismiss();
            }
        });
        codigo.show();
    }
}
