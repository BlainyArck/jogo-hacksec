package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GrupoHackerFerramentasActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_grupo_hacker_comunicacao, btn_grupo_hacker_hackers, btn_atacar_alvo, btn_updates;
    private TextView label_nome, label_tag, label_fundador, label_reputacao,
            label_ranking, label_membros;
    DatabaseReference mDatabase;

    private ImageView btn_firewall, btn_ddos_protection, btn_criptografia,
            btn_exploit, btn_ddos, btn_decrypt;
    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupo_hacker_ferramentas);

        btn_grupo_hacker_comunicacao = findViewById(R.id.btn_comunicacao);
        btn_grupo_hacker_hackers = findViewById(R.id.btn_grupo_hacker);
        btn_updates = findViewById(R.id.btn_updates);

        label_nome = findViewById(R.id.label_titulo);
        label_tag = findViewById(R.id.label_tag);
        label_fundador = findViewById(R.id.label_fundador);
        label_reputacao = findViewById(R.id.label_reputacao);
        label_ranking = findViewById(R.id.label_ranking);
        label_membros = findViewById(R.id.label_membros);

        btn_firewall = findViewById(R.id.btn_firewall);
        btn_ddos_protection = findViewById(R.id.btn_ddos_protection);
        btn_criptografia = findViewById(R.id.btn_criptografia);

        btn_exploit = findViewById(R.id.btn_exploits);
        btn_ddos = findViewById(R.id.btn_ddos);
        btn_decrypt = findViewById(R.id.btn_decrypt);
        btn_atacar_alvo = findViewById(R.id.btn_atacar_alvo);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layoutferramentas);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        buscarGrupoHacker();

        btn_updates.setOnClickListener(this);
        btn_grupo_hacker_comunicacao.setOnClickListener(this);
        btn_grupo_hacker_hackers.setOnClickListener(this);
        btn_atacar_alvo.setOnClickListener(this);

        btn_firewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                        TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                        TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                        TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                        ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                        label_nome_ferramenta.setText("Firewall");
                        label_level_ferramenta.setText("100");
                        label_custo_ferramenta.setText("100");
                        img.setImageResource(R.drawable.icone_firewall);

                myDialog.show();
            }
        });

        btn_ddos_protection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("DDoS Protection");
                label_level_ferramenta.setText("100");
                label_custo_ferramenta.setText("100");
                img.setImageResource(R.drawable.icone_antiddos);

                myDialog.show();
            }
        });

        btn_criptografia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Criptografia");
                label_level_ferramenta.setText("100");
                label_custo_ferramenta.setText("100");
                img.setImageResource(R.drawable.icone_criptografia);

                myDialog.show();
            }
        });

        btn_exploit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Exploit");
                label_level_ferramenta.setText("100");
                label_custo_ferramenta.setText("100");
                img.setImageResource(R.drawable.icone_exploit);

                myDialog.show();
            }
        });

        btn_ddos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("DDoS");
                label_level_ferramenta.setText("100");
                label_custo_ferramenta.setText("100");
                img.setImageResource(R.drawable.icone_ddos);

                myDialog.show();
            }
        });

        btn_decrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn_fechar;
                btn_fechar = myDialog.findViewById(R.id.btn_fechar);

                btn_fechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });

                TextView label_nome_ferramenta = myDialog.findViewById(R.id.label_nome_ferramenta);
                TextView label_level_ferramenta = myDialog.findViewById(R.id.label_level_ferramenta);
                TextView label_custo_ferramenta = myDialog.findViewById(R.id.label_custo_ferramenta);
                ImageView img = myDialog.findViewById(R.id.img_ferramenta);

                label_nome_ferramenta.setText("Decrypt");
                label_level_ferramenta.setText("100");
                label_custo_ferramenta.setText("100");
                img.setImageResource(R.drawable.icone_decrypt);

                myDialog.show();
            }
        });
    }

    public void buscarGrupoHacker(){
        mDatabase = FirebaseDatabase.getInstance().getReference("GrupoHacker").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String nome = dataSnapshot.child("nome").getValue().toString();
                String tag = dataSnapshot.child("tag").getValue().toString();
                String fundador = dataSnapshot.child("fundador").getValue().toString();
                String ranking = dataSnapshot.child("ranking").getValue().toString();
                String membros = dataSnapshot.child("membros").getValue().toString();
                String reputacao = dataSnapshot.child("reputacao").getValue().toString();

                label_nome.setText(nome);
                label_tag.setText(tag);
                label_fundador.setText(fundador);
                label_ranking.setText(ranking);
                label_membros.setText(membros);
                label_reputacao.setText(reputacao);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_updates:
                startActivity(new Intent(this, GrupoHackerUpdatesActivity.class));
                finish();
            break;
            case R.id.btn_atacar_alvo:
                startActivity(new Intent(this, GrupoHackerAlvoActivity.class));
                finish();
            break;
            case R.id.btn_comunicacao:
                startActivity(new Intent(this, GrupoHackerComunicacaoActivity.class));
                finish();
            break;
            case R.id.btn_grupo_hacker:
                startActivity(new Intent(this, GrupoHackerActivity.class));
                finish();
            break;
        }
    }
}
