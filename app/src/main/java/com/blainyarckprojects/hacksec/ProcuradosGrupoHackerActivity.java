package com.blainyarckprojects.hacksec;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ProcuradosGrupoHackerActivity extends AppCompatActivity implements View.OnClickListener {

    private ListView listview_grupohacker;
    private ImageView btn_hackers, btn_legends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_procurados_grupo_hacker);

        listview_grupohacker = findViewById(R.id.listview_grupohacker);
        btn_hackers = findViewById(R.id.btn_hackers);
        btn_legends =findViewById(R.id.btn_legends);

        int[] imgs = {R.drawable.logo_crew, R.drawable.logo_crew, R.drawable.logo_crew, R.drawable.logo_crew};
        String[] list_ranking = {"1", "2", "3", "4"};
        String[] list_grupohacker = {"NewUnionBR", "NewUnionBRT", "NewUnionLegends", "NewUnionEmpire"};
        String[] list_membros = {"20", "20", "20", "20"};
        String[] list_reputacao = {"1800", "1800", "1800", "1800"};

        MyAdapter adapter = new MyAdapter(this, imgs, list_ranking, list_grupohacker, list_membros, list_reputacao);

        listview_grupohacker.setAdapter(adapter);

        btn_hackers.setOnClickListener(this);
        btn_legends.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_hackers:
                startActivity(new Intent(this, ProcuradosActivity.class));
                finish();
            break;
            case R.id.btn_legends:
                startActivity(new Intent(this, ProcuradosLegendsActivity.class));
                finish();
            break;
        }
    }


    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        int[] imgs;
        String list_ranking[];
        String list_grupohacker[];
        String list_membros[];
        String list_reputacao[];


        MyAdapter(Context c, int[] imgs, String[] ranking, String[] grupohacker, String[] membros, String[] reputacao){
            super(c, R.layout.listview_procuradosgh, R.id.label_grupohacker, grupohacker);
            this.context = c;
            this.imgs = imgs;
            this.list_ranking = ranking;
            this.list_grupohacker = grupohacker;
            this.list_membros = membros;
            this.list_reputacao = reputacao;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_procuradosgh, parent, false);

            ImageView images = row.findViewById(R.id.img);
            TextView ranking = row.findViewById(R.id.label_ranking);
            TextView grupohacker = row.findViewById(R.id.label_grupohacker);
            TextView membros = row.findViewById(R.id.label_membros);
            TextView reputacao = row.findViewById(R.id.label_rep);

            images.setImageResource(imgs[position]);
            ranking.setText(list_ranking[position]);
            grupohacker.setText(list_grupohacker[position]);
            membros.setText(list_membros[position]);
            reputacao.setText(list_reputacao[position]);
            return row;
        }
    }
}
