package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NovoContratoActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_mnegro_trabalho, btn_mnegro_mercado, btn_contratos, btn_criar_contrato;
    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_contrato);

        btn_mnegro_trabalho = findViewById(R.id.btn_mnegro_trabalho);
        btn_mnegro_mercado = findViewById(R.id.btn_mnegro_mercado);
        btn_contratos = findViewById(R.id.btn_contratos);
        btn_criar_contrato = findViewById(R.id.btn_criar_contrato);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layoutcontratos);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        btn_mnegro_trabalho.setOnClickListener(this);
        btn_mnegro_mercado.setOnClickListener(this);
        btn_contratos.setOnClickListener(this);
        btn_criar_contrato.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_mnegro_trabalho:
                startActivity(new Intent(this, TrabalhoActivity.class));
                finish();
            break;
            case R.id.btn_mnegro_mercado:
                startActivity(new Intent(this, MercadoNegroActivity.class));
                finish();
            break;
            case R.id.btn_contratos:
                startActivity(new Intent(this, ContratosActivity.class));
                finish();
            break;
            case R.id.btn_criar_contrato:
                buttonCriarContrato();
            break;
        }
    }

    private void buttonCriarContrato() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        myDialog.show();
    }
}
