package com.blainyarckprojects.hacksec;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class DesafioMensalActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_desafio_diario, btn_desafio_semanal, btn_recompensa;
    ListView listview_desafio_mensal;
    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desafio_mensal);

        String[] list_desafios = {"Explorar dispositivos", "Ativar botnet", "Fazer ataque defacement", "Fazer atque MITM",
                "Fazer ataque phishing", "Fazer ataque DDoS", "Fazer ataque Ransomware", "Limpar rastros", "Descriptografar Carteira",
                "Efetuar Roubos"};
        String[] list_realizados = {"0/1500", "0/1500", "0/1500", "0/1500", "0/1500", "0/1500", "0/1500", "0/1500", "0/1500", "0/1500"};

        btn_desafio_diario = findViewById(R.id.btn_desafio_diario);
        btn_desafio_semanal = findViewById(R.id.btn_desafio_semanal);
        listview_desafio_mensal = findViewById(R.id.listview_desafio_mensal);
        btn_recompensa = findViewById(R.id.btn_recompensa);

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.layoutrecompensa);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        MyAdapter adapter = new MyAdapter(this, list_desafios, list_realizados);

        listview_desafio_mensal.setAdapter(adapter);

        btn_desafio_diario.setOnClickListener(this);
        btn_desafio_semanal.setOnClickListener(this);
        btn_recompensa.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_desafio_diario:
                startActivity(new Intent(this, DesafiosActivity.class));
                finish();
            break;
            case R.id.btn_desafio_semanal:
                startActivity(new Intent(this, DesafioSemanalActivity.class));
                finish();
            break;
            case R.id.btn_recompensa:
                mostrarRecompensa();
            break;
        }
    }

    private void mostrarRecompensa() {
        Button btn_fechar;
        btn_fechar = myDialog.findViewById(R.id.btn_fechar);

        btn_fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        TextView btc = myDialog.findViewById(R.id.label_btc);
        TextView boosters = myDialog.findViewById(R.id.label_boosters);
        TextView money = myDialog.findViewById(R.id.label_money);

        btc.setText("3.000");
        boosters.setText("3.000");
        money.setText("30.000");

        myDialog.show();
    }

    class MyAdapter extends ArrayAdapter<String> {
        Context context;
        String[] list_desafios;
        String[] list_realizados;

        MyAdapter(Context c, String[] desafios, String[] realizados){
            super(c, R.layout.listview_desafios, R.id.label_acoes, desafios);
            this.context = c;
            this.list_desafios = desafios;
            this.list_realizados = realizados;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.listview_perfil, parent, false);

            TextView acoes = row.findViewById(R.id.label_acoes);
            TextView realizados = row.findViewById(R.id.label_realizados);

            acoes.setText(list_desafios[position]);
            realizados.setText(list_realizados[position]);

            return row;
        }
    }
}
